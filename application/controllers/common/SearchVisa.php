<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchVisa extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_dashboard');
	 $this->load->helper('url');
    }
    public function index(){
        $in = $this->session->flashdata('pass');
        $hasil = $this->session->flashdata('on');
        $data['header_title'] = 'Search Visa - '.$in;
		$data['breadchumb'] = 'Search'; 
        $data['hasil_search'] = $hasil;
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['jenis_visa'] = $this->m_dashboard->get_jenis_visa()->result();
        $data['negara'] = $this->m_dashboard->get_negara()->result();
        $this->load->view('common/v_search_visa', $data);
    }
    
}