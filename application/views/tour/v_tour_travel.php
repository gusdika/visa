<?
$header;
$sidebar;
?>
<!-- Content Wrapper. Contains page content -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0 text-dark"><?php echo $header_title?></h5>
          </div>

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="<?echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                          <?php if (!empty($this->session->flashdata('error'))){?>
                            <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('error');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?} else if(!empty($this->session->flashdata('success'))) {?>
                            <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?}?>

                          <div id="input-form-visa">
                            <div class="form-grid-container">
                                <form action="<?php echo base_url(). 'tour/TourTravel/add_tour_travel'; ?>" method="post">
                                        <div class="row">
                                                <div class="form-group col-md-6">
                                                    <div style="display:flex">
                                                        <label >Dari</label>
                                                        <div class="text-danger">*</div>
                                                    </div>
                                                        <input type="date" name="dari" class="form-control">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <div style="display:flex">
                                                        <label >Sampai</label>
                                                        <div class="text-danger">*</div>
                                                    </div>
                                                            <input type="date" name="sampai" class="form-control">
                                                </div>
                                                <div class="form-group col-md-12">
                                                    <div style="display:flex">
                                                        <label >Name</label>
                                                        <div class="text-danger">*</div>
                                                    </div>
                                                         <input type="text" class="form-control" name="nama" placeholder="Enter name">
                                              </div>
                                              <div class="form-group col-md-6">
                                                    <div style="display:flex">
                                                        <label >No Paspor</label>
                                                        <div class="text-danger">*</div>
                                                    </div>
                                                    <input type="text" class="form-control" name="no_paspor" placeholder="Masukan no paspor" id="email">
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <div style="display:flex">
                                                        <label>Negara</label>
                                                    </div>
                                                        <div class="dropdown">
                                                                    <select class="form-control" name="negara" required autocomplete="off">
                                                                        <option selected>Pilih Negara</option>
                                                                    <?php foreach($negara as $result){?>
                                                                        <option value="<?php echo $result->id?>"><?php echo $result->country_name?></option>
                                                                    <? } ?>
                                                                    </select>
                                                        </div>
                                               </div>
                                               <div class="form-group col-md-6">
                                                    <label >Alamat</label>
                                                    <textarea type="text" class="form-control" name="alamat" placeholder="Masukan alamat"></textarea>
                                              </div>
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="pwd">Jenis Tour</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <div class="dropdown">
                                                        <select class="form-control" name="id_tour" required autocomplete="off">
                                                                <option selected>Pilih Tour</option>
                                                            <?php foreach($jenis_tour as $result){?>
                                                                <option value="<?php echo $result->id_jenis_tour?>"><?php echo $result->nama_tour?></option>
                                                            <? } ?>
                                                        </select>
                                                </div>
                                              </div>
                                              <div class="form-group col-md-12">
                                                    <label >Keterangan</label>
                                                    <textarea type="text" class="form-control" name="keterangan" placeholder="Masukan keterangan"></textarea>
                                              </div>
                                              <div class="row perhitungan">
                                                    <div class="form-group col-md-6">
                                                    <label for="birthday">Total Biaya</label>
                                                        <input type="text" class="total form-control" name="total_biaya" id="total" >
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="birthday">DP</label>
                                                            <input type="text" class="dp form-control" name="dp" placeholder="DP" id="bayar" >
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label for="birthday">Sisa</label>
                                                        <input type="text" class="sisa form-control" name="sisa" placeholder="Sisa" id="sisa" readonly="readonly">
                                                    </div>
                                              </div>
                                            <div class="button-submit-container">
                                                <button type="submit" class="btn btn-primary btn-submit-visa">Submit</button>
                                            </div>
                                         </div><!--end row -->
                                </form>
                            </div>
                          </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <div id="list-visa">
                                <div class="table-responsive">
                                    <table class="table table-bordered" >
                                        <thead>
                                            <tr>
                                                <th scope="col" >No</th>
                                                <th scope="col" >Name</th>
                                                <th scope="col" >Dari - Sampai</th>
                                                <th scope="col" >No Paspor</th>
                                                <th scope="col" >Negara</th>
                                                <th scope="col" >Alamat</th>
                                                <th scope="col" >Jenis Tour</th>
                                                <th scope="col" >Keterangan</th>
                                                <th scope="col" >Total</th>
                                                <th scope="col" >DP</th>
                                                <th scope="col" >Sisa</th>
                                                <th scope="col" >Aksi</th>


                                            </tr>
                                        </thead>
                                    <caption><a href="<?php echo base_url(). 'dashboard/list_visa'; ?>">Lihat Semua Data Tour Travel</a></caption>

                                        <tbody>
                                            <?php if(!empty($this->session->flashdata('last_added'))){ ?>
                                                    <?php 
                                                    $no = 1;
                                                    foreach($this->session->flashdata('last_added') as $data){ 
                                                    ?>
                                                        <tr>
                                                        <th scope="row"><?echo $no?></th>
                                                        <td><?php echo $data['nama']?></td>
                                                        <td><?php echo  $data['tgl_dari']?> - <?php echo  $data['tgl_sampai']?></td>
                                                        <td><?php echo  $data['no_paspor']?></td>
                                                        <td><?php echo  $data['country_name']?></td>
                                                        <td><?php echo  $data['alamat']?></td>
                                                        <td><?php echo  $data['nama_tour']?></td>
                                                        <td><?php echo  $data['keterangan']?></td>
                                                        <td><?php echo  number_format($data['total_biaya'],2,',','.')?></td>
                                                        <td><?php echo  number_format($data['dp'],2,',','.')?></td>
                                                        <td><?php echo  number_format($data['sisa'],2,',','.')?></td>
                                                        <td>
                                                        <div class="button-aksi-container">
                                                            <button type="button" class="btn btn-info btn-aksi" data-toggle="modal" data-target="#modal-edit<?=$data['id_tt'];?>"><i class="far fa-edit"></i></button>
                                                            <button type="submit" class="btn btn-danger btn-aksi" data-toggle="modal" data-target="#modal_hapus<?php echo $data['id_tt'];?>"><i class="fas fa-trash-alt"></i></button>
                                                            <form action="<?php echo base_url(). 'tour/ListTourTravel/pdf'; ?>" method="post">
                                                                <input type="hidden" class="form-control" name="id" value="<?php echo $data['id_tt'];?>"  />
                                                                <button type="submit" class="btn btn-warning btn-aksi" data-toggle="modal"><i class="fas fa-file"></i></button>
                                                            </form>
                                                        </div>
                                                        </td>
                                                        </tr>
                                                        <tr>
                                                    <? $no++; }?>
                                            <? }?>
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                </div>
    </div> <!-- last row end -->

<?
$footer;
$script;
?>
<script type="text/javascript">
$( '.total' ).mask('0.000.000.000', {reverse: true});

$( '.dp' ).mask('0.000.000.000', {reverse: true});

$( '.sisa' ).mask('0.000.000.000', {reverse: true});

$(document).ready(function(){
  $('.perhitungan').keyup(function(){
      var total = $("#total").val()
			var bayar = $("#bayar").val()
			var x =  total.replace(/\./g,'');
			var y =  bayar.replace(/\./g,'');
			var sisa = x - y;
      console.log(sisa);
			$('#sisa').val(sisa); 

    });
  })

$(document).ready(function(){
  $('.calchange').keyup(function(){
      var total = $("#total-modal").val()
			var bayar = $("#bayar-modal").val()
			var x =  total.replace(/\./g,'');
			var y =  bayar.replace(/\./g,'');
			var sisa = x - y;
      console.log(sisa);
			$('#sisa-modal').val(sisa); 

    });
  })

</script>