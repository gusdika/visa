<? $header;
   $sidebar;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0 text-dark"><?php echo $header_title?></h5>
          </div>


          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="<?echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

        <div class="row">
                          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                          <?php if (!empty($this->session->flashdata('error'))){?>
                            <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('error');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?} else if(!empty($this->session->flashdata('success'))) {?>
                            <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?}?>
                              <div id="input-form-visa">
                                <div class="form-grid-container">
                                  <form action="<?php echo base_url(). 'common/InputVisa/add_visa'; ?>" method="post">
                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="email">Name</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <input type="text" class="form-control" name="nama" placeholder="Masukan name" id="email">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="pwd">Jenis Visa</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <div class="dropdown">
                                                        <select class="form-control" name="jenis_visa" required autocomplete="off">
                                                                <option selected>Pilih Jenis</option>
                                                            <?php foreach($jenis_visa as $result){?>
                                                                <option value="<?php echo $result->id?>"><?php echo $result->nama_visa?></option>
                                                            <? } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                    <label for="email">No Paspor</label>
                                                    <div class="text-danger">*</div>
                                                </div>
                                                <input type="text" class="form-control" name="no_paspor" placeholder="Masukan no paspor">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="pwd">Email</label>
                                                <input type="text" class="form-control" name="email" placeholder="Masukan email" id="pwd">
                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                          <div class="form-group col-md-6">
                                            <div style="display:flex">
                                              <label for="birthday">Expired Visa 1</label>
                                              <div class="text-danger">*</div>
                                            </div>
                                            <input type="date" name="expired_v1" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                          <div style="display:flex">
                                              <label for="birthday">Expired Visa 2</label>
                                              <div class="text-danger">*</div>
                                          </div>
                                                <input type="date" name="expired_v2" class="form-control">
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="form-group col-md-6">
                                          <div style="display:flex">
                                            <label for="birthday">Jdwal Foto</label>
                                            <div class="text-danger">*</div>
                                          </div>
                                            <input type="date" name="jadwal_foto" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                              <div style="display:flex">
                                                  <label for="pwd">Negara</label>
                                                </div>
                                                <div class="dropdown">
                                                            <select class="form-control" name="negara" required autocomplete="off">
                                                                <option selected>Pilih Negara</option>
                                                            <?php foreach($negara as $result){?>
                                                                <option value="<?php echo $result->id?>"><?php echo $result->country_name?></option>
                                                            <? } ?>
                                                            </select>
                                                </div>
                                            </div>
                                      </div>

                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <label for="email">Keterangan</label>
                                                <input type="text" class="form-control" name="keterangan" placeholder="Enter nik" id="email"></textarea>
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="pwd">Phone/WA</label>
                                                <input type="number" class="form-control" name="telp" placeholder="Enter tlp" id="pwd">
                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                              <div class="form-group col-md-12">
                                                <label for="email">Alamat</label>
                                                <textarea type="text" class="form-control" name="alamat" placeholder="Enter address" id="email"></textarea>
                                              </div>
                                      </div>
                                     
                                      <div class="row perhitungan">
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Tanggal Transaksi</label>
                                            <input type="date" name="tgl_transaksi" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                          <label for="birthday">Total Biaya</label>
                                                <input type="text" class="total form-control" name="total_biaya" id="total" >
                                          </div>
                                        <div class="form-group col-md-6">
                                          <label for="birthday">DP</label>
                                                <input type="text" class="dp form-control" name="dp" placeholder="DP" id="bayar" >
                                        </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">Sisa</label>
                                                <input type="text" class="sisa form-control" name="sisa" placeholder="Sisa" id="sisa" readonly="readonly">
                                          </div>
                                    </div>
                                      <div class="button-submit-container">
                                            <button type="submit" class="btn btn-primary btn-submit-visa">Submit</button>
                                      </div>
                                  </form>
                                  </div>
                              </div>
                          <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                              <div id="list-visa">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                    <thead>
                                      <tr>
                                        <th scope="col" >No</th>
                                        <th scope="col" >Name</th>
                                        <th scope="col" >No Paspor</th>
                                        <th scope="col" >Jenis Visa</th>
                                        <th scope="col" >Email</th>
                                        <th scope="col" >Negara</th>
                                        <th scope="col" >Exp 1</th>
                                        <th scope="col" >Exp 2</th>
                                        <th scope="col" >Jadwal Foto</th>
                                        <th scope="col" >Alamat</th>
                                        <th scope="col" >Phone</th>
                                        <th scope="col" >Transaksi</th>
                                        <th scope="col" >Total</th>
                                        <th scope="col" >DP</th>
                                        <th scope="col" >Sisa</th>
                                        <th scope="col" >Keterangan</th>
                                        <th scope="col" >Aksi</th>


                                      </tr>
                                    </thead>
                                    <caption><a href="<?php echo base_url(). 'dashboard/list_visa'; ?>">Lihat Semua Data Visa</a></caption>

                                    <tbody>
                                  <?php if(!empty($this->session->flashdata('last_added'))){ ?>
                                          <?php 
                                          $no = 1;
                                          foreach($this->session->flashdata('last_added') as $data){ 
                                          ?>
                                              <tr>
                                              <th scope="row"><?echo $no?></th>
                                              <td><?php echo $data['nama']?></td>
                                              <td><?php echo  $data['no_paspor']?></td>
                                              <td><?php echo  $data['nama_visa']?></td>
                                              <td><?php echo  $data['email']?></td>
                                              <td><?php echo  $data['country_name']?></td>
                                              <td><?php echo  $data['expired_v1']?></td>
                                              <td><?php echo  $data['expired_v2']?></td>
                                              <td><?php echo  $data['jadwal_foto']?></td>
                                              <td><?php echo  $data['alamat']?></td>
                                              <td><?php echo  $data['telp']?></td>
                                              <td><?php echo  $data['tgl_transaksi']?></td>
                                              <td><?php echo  number_format($data['total_biaya'],2,',','.')?></td>
                                              <td><?php echo  number_format($data['dp'],2,',','.')?></td>
                                              <td><?php echo  number_format($data['sisa'],2,',','.')?></td>
                                              <td><?php echo  $data['keterangan']?></td>
                                              <td>
                                              <div class="button-aksi-container">
                                                  <button type="button" class="btn btn-info btn-aksi" data-toggle="modal" data-target="#modal-edit<?=$data['id_visa'];?>"><i class="far fa-edit"></i></button>
                                                  <button type="submit" class="btn btn-danger btn-aksi" data-toggle="modal" data-target="#modal_hapus<?php echo $data['id_visa'];?>"><i class="fas fa-trash-alt"></i></button>
                                                  <form action="<?php echo base_url(). 'common/ListVisa/pdf'; ?>" method="post">
                                                    <input type="hidden" class="form-control" name="id" value="<?php echo $data['id_visa'];?>"  />
                                                    <button type="submit" class="btn btn-warning btn-aksi" data-toggle="modal"><i class="fas fa-file"></i></button>
                                                </form>
                                              </div>
                                              </td>
                                              </tr>
                                              <tr>
                                          <? $no++; }?>
                                  <? }?>
                              
                                    </tbody>
                                    </table>
                                </div>
                              </div>
                          </div>
                      </div>
                </div>
</div>
<?php if(!empty($this->session->flashdata('last_added'))){ ?>
<?php $no=0; foreach($this->session->flashdata('last_added') as $row): $no++; ?>
<div class="modal fade" id="modal-edit<?=$row['id_visa']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Visa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?php echo base_url(). 'common/InputVisa/edit_visa'; ?>" method="post">
                <div class="row">
                <input type="hidden" class="form-control" name="id" value="<?php echo $row['id_visa']?>"  />
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="email">Name</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <input type="text" class="form-control" value="<?php echo $row['nama']?>" name="nama" placeholder="Enter first name" id="email">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="pwd">Jenis Visa</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <div class="dropdown">
                                                        <select class="form-control" name="jenis_visa" required autocomplete="off">
                                                                <option value="<?php echo $row['id_jenis_visa']?>" selected><?php echo $row['nama_visa']?></option>
                                                            <?php foreach($jenis_visa as $result){?>
                                                                <option value="<?php echo $result->id?>"><?php echo $result->nama_visa?></option>
                                                            <? } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                    <label for="email">No Paspor</label>
                                                    <div class="text-danger">*</div>
                                                </div>
                                                <input type="text" class="form-control" name="no_paspor" value="<?php echo $row['no_paspor']?>" placeholder="Enter no visa" id="email">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="pwd">Email</label>
                                                <input type="text" class="form-control" name="email" value="<?php echo $row['email']?>" placeholder="Enter work" id="pwd">
                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Expired Visa 1</label>
                                            <input type="date" name="expired_v1" value="<?php echo $row['expired_v1']?>" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                          <label for="birthday">Expired Visa 2</label>
                                                <input type="date" name="expired_v2" value="<?php echo $row['expired_v2']?>" class="form-control">
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Jdwal Foto</label>
                                            <input type="date" name="jadwal_foto" value="<?php echo $row['jadwal_foto']?>" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                              <div style="display:flex">
                                                  <label for="pwd">Negara</label>
                                                </div>
                                                <div class="dropdown">
                                                            <select class="form-control" name="negara" required autocomplete="off">
                                                                <option value="<?php echo $row['id']?>" selected><?php echo $row['country_name']?></option>
                                                            <?php foreach($negara as $result){?>
                                                                <option value="<?php echo $result->id?>"><?php echo $result->country_name?></option>
                                                            <? } ?>
                                                            </select>
                                                </div>
                                            </div>
                                      </div>

                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <label for="email">Keterangan</label>
                                                <input type="text" class="form-control" name="keterangan" value="<?php echo $row['keterangan']?>"  placeholder="Enter nik"></textarea>
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="pwd">Phone/WA</label>
                                                <input type="number" class="form-control" name="telp" value="<?php echo $row['telp']?>" placeholder="Enter tlp" id="pwd">
                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                              <div class="form-group col-md-12">
                                                <label for="email">Alamat</label>
                                                <textarea type="text" class="form-control" name="alamat" placeholder="Enter address"><?php echo $row['alamat']?></textarea>
                                              </div>
                                      </div>
                                     
                                      <div class="row calchange">
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Tanggal Transaksi</label>
                                            <input type="date" name="tgl_transaksi" value="<?php echo $row['tgl_transaksi']?>" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                          <label for="birthday">Total Biaya</label>
                                                <input type="text" class="form-control total" value="<?php echo $row['total_biaya']?>" name="total_biaya" id="total-modal">
                                          </div>
                                        <div class="form-group col-md-6">
                                          <label for="birthday">DP</label>
                                                <input type="text" class="form-control dp" value="<?php echo $row['dp']?>" name="dp" placeholder="DP" id="bayar-modal">
                                        </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">Sisa</label>
                                                <input type="text" class="form-control sisa" value="<?php echo $row['sisa']?>" name="sisa" placeholder="Sisa" id="sisa-modal" readonly="readonly">
                                          </div>
                                    </div>
                            <div class="row">
                                    <div class="form-group col-md-6">
                                    <label for="pwd">Status</label>
                                    <label class="switch">
                                    <?php if($row['status'] == 1){?>
                                    <input type="checkbox" name="status[]" checked>
                                    <span class="slider round"></span>
                                <? }else { ?>
                                    <input type="checkbox" name="status[]">
                                    <span class="slider round"></span>
                                    <? }?>
                                    </label>
                                </div>
                            </div>
                
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </form>
      </div>

    </div>
  </div>
</div>
<?php endforeach; ?>
<?}?>

<?php if(!empty($this->session->flashdata('last_added'))){ ?>
<?php $no=0; foreach($this->session->flashdata('last_added') as $row): $no++; ?>
<div class="modal fade" id="modal_hapus<?php echo $row['id_visa'];?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'common/InputVisa/delete_visa'; ?>">
            <input type="hidden" class="form-control" name="id" value="<?php echo $row['id_visa'];?>"  />

                <div class="modal-body">
                    <p>Anda yakin mau menghapus <b><?php echo $row['nama'];?></b></p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach; ?>
<?}?>
<? $footer;
   $script;
?>
<script type="text/javascript">
$( '.total' ).mask('0.000.000.000', {reverse: true});

$( '.dp' ).mask('0.000.000.000', {reverse: true});

$( '.sisa' ).mask('0.000.000.000', {reverse: true});

$(document).ready(function(){
  $('.perhitungan').keyup(function(){
      var total = $("#total").val()
			var bayar = $("#bayar").val()
			var x =  total.replace(/\./g,'');
			var y =  bayar.replace(/\./g,'');
			var sisa = x - y;
      console.log(sisa);
			$('#sisa').val(sisa); 

    });
  })

$(document).ready(function(){
  $('.calchange').keyup(function(){
      var total = $("#total-modal").val()
			var bayar = $("#bayar-modal").val()
			var x =  total.replace(/\./g,'');
			var y =  bayar.replace(/\./g,'');
			var sisa = x - y;
      console.log(sisa);
			$('#sisa-modal').val(sisa); 

    });
  })


</script>