<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiket extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
     $this->load->model('m_tiket');
	 $this->load->model('m_dashboard');
     
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Input Tiket';
		$data['breadchumb'] = 'Tiket'; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
		$data['negara'] = $this->m_dashboard->get_negara()->result();
        $data['list_tiket_last'] = $this->m_tiket->get_list_tiket_last()->result();
        $data['jenis_maskapai'] = $this->m_tiket->get_master_jenis_maskapai()->result();
        
        $this->load->view('tiket/v_tiket', $data);
    }

    public function add_tiket(){
		if(!empty($this->input->post('id_maskapai') && $this->input->post('nama') && $this->input->post('no_paspor') && $this->input->post('id_negara') )){
			$nama = $this->input->post('nama');
			$id_maskapai = $this->input->post('id_maskapai');
			$no_paspor = $this->input->post('no_paspor');
			$t_lahir = $this->input->post('t_lahir');
			$t_expired = $this->input->post('t_expired');
            $rute = $this->input->post('rute');
			$id_negara = $this->input->post('id_negara');
			$telp = $this->input->post('telp');
			$no_penerbangan = $this->input->post('no_penerbangan');
			$time_destinasi = $this->input->post('time_destination');
			$tiket_dewasa = $this->input->post('tiket_dewasa');
            $tiket_anak = $this->input->post('tiket_anak');
			$per_tiket = $this->input->post('per_tiket');
            $total = str_replace(".","",strtolower(trim($this->input->post('total'))));
            $dp = str_replace(".","",strtolower(trim($this->input->post('dp'))));
            $sisa = $this->input->post('sisa');
			if($total <= $dp) {
				$status = 1;
			} else {
				$status = 0;
			}
			$data = array(
				'nama' => $nama,
				'id_maskapai' => $id_maskapai,
				'no_paspor' => $no_paspor,
				't_lahir' => $t_lahir,
				't_expired' => $t_expired,
				'rute' => $rute,
				'telp' => $telp,
                'no_penerbangan' => $no_penerbangan,
                'id_negara'     => $id_negara,
				'time_destinasi' => $time_destinasi,
                'tiket_dewasa' => $tiket_dewasa,
                'tiket_anak'  => $tiket_anak,
                'per_tiket' => str_replace(".","",strtolower(trim($per_tiket))),
				'total' => $total,
				'dp' => $dp,
				'sisa' => str_replace(".","",strtolower(trim($sisa))),
				'status' => $status
                );
			$res = $this->m_tiket->model_add_tiket($data,'tb_tiket');
			$this->session->set_flashdata('success', "Tiket Berhasil Didaftarkan"); 
			$data_last = $this->m_tiket->get_list_tiket_last()->result();
			$data_last_view= json_decode(json_encode($data_last), true);
			$array = json_decode( json_encode($data_last), true);
			foreach ($array as $result) {
				$income = array(
					'jum' => $result['dp'],
					'id_tiket' => $result['id_tiket']
				);
				$masuk_kas = $this->m_dashboard->add_kas_masuk($income,'tb_income');
			}


			$this->session->set_flashdata('last_added',$data_last_view);
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('tiket/Tiket');
	}

	function edit_tiket(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$id_maskapai = $this->input->post('id_maskapai');
		$no_paspor = $this->input->post('no_paspor');
		$t_lahir = $this->input->post('t_lahir');
		$telp = $this->input->post('telp');
		$t_expired = $this->input->post('t_expired');
		$rute = $this->input->post('rute');
		$id_negara = $this->input->post('id_negara');
		$no_penerbangan = $this->input->post('no_penerbangan');
		$time_destinasi = $this->input->post('time_destination');
		$tiket_dewasa = $this->input->post('tiket_dewasa');
		$tiket_anak = $this->input->post('tiket_anak');
		$per_tiket = $this->input->post('per_tiket');
		$total = str_replace(".","",strtolower(trim($this->input->post('total'))));
		$dp = str_replace(".","",strtolower(trim($this->input->post('dp'))));
		$sisa = $this->input->post('sisa');
		if($total <= $dp) {
			$status = 1;
		} else {
			$status = 0;
		}
		$data = array(
			'nama' => $nama,
			'id_maskapai' => $id_maskapai,
			'no_paspor' => $no_paspor,
			't_lahir' => $t_lahir,
			't_expired' => $t_expired,
			'rute' => $rute,
			'telp' => $telp,
			'no_penerbangan' => $no_penerbangan,
			'id_negara'     => $id_negara,
			'time_destinasi' => $time_destinasi,
			'tiket_dewasa' => $tiket_dewasa,
			'tiket_anak'  => $tiket_anak,
			'per_tiket' => str_replace(".","",strtolower(trim($per_tiket))),
			'total' => $total,
			'dp' => $dp,
			'sisa' => str_replace(".","",strtolower(trim($sisa))),
			'status' => $status
			);

		$where = array(
			'id_tiket' => $id
		);

		$this->m_tiket->model_edit_tiket($where,$data,'tb_tiket');
		$this->m_dashboard->model_edit_kas_masuk_tiket($id, $dp);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Visa Dengan No " . $no_paspor . " Berhasil Diedit" );
			$data_last = $this->m_tiket->get_list_tiket_last()->result();
			$data_last_view= json_decode(json_encode($data_last), true);
			$this->session->set_flashdata('last_added',$data_last_view);
            
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('tiket/Tiket');

	}

	function delete_tiket(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id_tiket' => $id);
		$this->m_tiket->model_delete_tiket($where,'tb_tiket');
		$this->m_dashboard->modal_delete_kas_masuk_tiket($id);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('tiket/Tiket');
	}
	
	public function list_tiket(){
		redirect('tiket/ListTiket');
	}

	

}