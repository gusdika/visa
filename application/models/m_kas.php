<?php 
 
class M_kas extends CI_Model{

    function get_list_master_jenis_transaksi(){
        $data = $this->db->query('SELECT * FROM `tb_jenis_transaksi` WHERE 1');
        return $data;
    }

    function model_add_kas_keluar($data, $table){
        $this->db->insert($table,$data);
    }
    function get_list_kas_keluar(){
        $data = $this->db->query('SELECT * FROM `tb_outcome` ot LEFT JOIN tb_jenis_transaksi jt ON ot.id_transaksi = jt.id_transaksi');
        return $data;
    }
    function model_edit_kas_keluar($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }
    function model_delete_kas_keluar($where,$table){
        $this->db->where($where);
		$this->db->delete($table);
    }
    function get_list_kas_masuk(){
        $data = $this->db->query('SELECT * FROM tb_income  WHERE 1');
        return $data;
    }
}