<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KasMasuk extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_kas');
	 $this->load->helper('url');
    }
    function index(){
        $data['header_title'] = 'List Kas Masuk';
		$data['breadchumb'] = 'Kas Masuk'; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['list_kas_masuk'] = $this->m_kas->get_list_kas_masuk()->result();
        $this->load->view('kas/v_kas_masuk', $data);
    }

    function edit_kas_masuk(){

    }

    function delete_kas_masuk(){

    }

}