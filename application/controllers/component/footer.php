<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Footer extends CI_Controller {

	public function index()
	{
		// ----------------------------
		// format penulisan: array(
		//   'Teks|ikon|alamat_link', 
		// );
		// ----------------------------
		$this->load->view('component/footer.php');
		
	}
}
