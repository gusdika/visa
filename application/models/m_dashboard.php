<?php 
 
class M_dashboard extends CI_Model{
 
	function model_add_visa($data,$table){
		$this->db->insert($table,$data);
    }

    function add_kas_masuk($data, $table){
      $this->db->insert($table,$data);
    }
  
  function get_visa(){
        $data = $this->db->query('SELECT * FROM `tb_visa` tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE status = 1 ORDER BY id_visa DESC LIMIT 5');
        return $data;
  }
  function get_list_visa_last(){
    $data = $this->db->query('SELECT * FROM `tb_visa` tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id ORDER BY tv.id_visa DESC LIMIT 1');
    return $data;
  }
  function get_list_dp_view(){
    $data = $this->db->query('SELECT dp FROM `tb_visa` ORDER BY id_visa DESC LIMIT 1');
    return $data;
  } 
  function get_list_visa(){
    $data = $this->db->query('SELECT * FROM `tb_visa` tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id');
    return $data;
  }
  function get_list_active(){
    $data = $this->db->query('SELECT * FROM tb_visa WHERE status = 1');
    return $data;
  }
  function get_list_unactive(){
    $data = $this->db->query('SELECT * FROM tb_visa WHERE status = 0');
    return $data;
  }
  function model_edit_visa($where,$data,$table){
    $this->db->where($where);
    $this->db->update($table,$data);
  }
  function model_edit_kas_masuk($id, $total){
    $data = $this->db->query('UPDATE tb_income SET jum = '. $total .' WHERE id_visa = '. $id);
    return $data;
  }
  function modal_delete_kas_masuk($id){
    $data = $this->db->query('DELETE FROM tb_income WHERE id_visa = '.$id);
    return $data;
  }
  function model_edit_kas_masuk_tiket($id, $total){
    $data = $this->db->query('UPDATE tb_income SET jum = '. $total .' WHERE id_tiket = '. $id);
    return $data;
  }
  function modal_delete_kas_masuk_tiket($id){
    $data = $this->db->query('DELETE FROM tb_income WHERE id_tiket = '.$id);
    return $data;
  }
  function model_delete_visa($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
  }

  
  function model_search_visa($data) {
      $search = $this->db->query('SELECT * FROM tb_visa tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE no_paspor LIKE "%' . $data . '%" OR nama LIKE "%' . $data . '%"');
      return $search;
  }

  function get_jenis_visa(){
    $data = $this->db->query('SELECT * FROM tb_jenis_visa WHERE 1');
    return $data;
  }

  function get_negara(){
    $data = $this->db->query('SELECT * FROM apps_countries WHERE 1');
    return $data;
  }

  function get_kas_masuk(){
    $data = $this->db->query('SELECT SUM(jum) AS jumlah FROM tb_income WHERE 1');
    return $data;
  }
  function get_kas_keluar(){
    $data = $this->db->query('SELECT SUM(total) AS total FROM tb_outcome WHERE 1');
    return $data;
  }

  function model_search_by($data){
    $search = $this->db->query('SELECT * FROM tb_visa tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE status = ' . $data . '');
      return $search;
  }
  function model_search_date($data){
    $search = $this->db->query("SELECT * FROM tb_visa tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE tv.jadwal_foto = '". $data . "'");
      return $search;
  }

  function model_search_month_year($month, $year){
    $search = $this->db->query("SELECT * FROM tb_visa tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE MONTH(tgl_transaksi) = '" . $month . "' AND YEAR(tgl_transaksi) = '" . $year . "'");
      return $search;
  }
  function get_list_kas_keluar_jum(){
    $data = $this->db->query('SELECT tj.nama_transaksi, SUM(tw.total) AS keluar_tot FROM tb_outcome tw LEFT JOIN tb_jenis_transaksi tj ON tw.id_transaksi = tj.id_transaksi GROUP BY tw.id_transaksi');
    return $data;
  }
  function get_list_kas_masuk_jum_visa(){
    $data = $this->db->query('SELECT id_visa, SUM(jum) AS tot_visa FROM tb_income ti WHERE id_visa != 0');
    return $data;
  }
  function get_list_kas_masuk_jum_tiket(){
    $data = $this->db->query('SELECT id_tiket, SUM(jum) AS tot_tiket FROM tb_income ti WHERE id_tiket != 0');
    return $data;
  }
  function get_week_jadwal_foto(){
    $data = $this->db->query('SELECT * FROM tb_visa WHERE YEARWEEK(jadwal_foto) = YEARWEEK(NOW()) ORDER BY id_visa ASC');
    return $data;
  }


  function get_list_visa_pdf($id){
    $data = $this->db->query('SELECT * FROM `tb_visa` tv LEFT JOIN tb_jenis_visa tj ON tv.id_jenis_visa = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE id_visa = ' . $id);
    return $data;
  }

}