<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisTour extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_master');
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Input Master Jenis Tour';
		$data['breadchumb'] = 'Master Tour'; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['list_master_jenis_tour'] = $this->m_master->get_list_master_jenis_tour()->result();

        $this->load->view('common/v_master_jenis_tour', $data);
    }

    public function add_jenis_tour(){
		if(!empty($this->input->post('nama_tour') || $this->input->post('deskripsi'))){
			$nama_tour = $this->input->post('nama_tour');
            $deskripsi = $this->input->post('deskripsi');
			$data = array(
				'nama_tour' => $nama_tour,
                'deskripsi' => $deskripsi,
				);

			$res = $this->m_master->model_add_jenis_tour($data,'tb_jenis_tour');
			$this->session->set_flashdata('success', "Maskapai Berhasil Didaftarkan"); 
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('common/JenisTour');
    }

    public function edit_jenis_tour(){
        $this->db->trans_start();
		$this->load->helper('url');
		$id = $this->input->post('id');
        $nama_tour = $this->input->post('nama_tour');
        $deskripsi = $this->input->post('deskripsi');

        $data = array(
            'nama_tour' => $nama_tour,
            'deskripsi' => $deskripsi,
            );
        
        $where = array(
            'id_jenis_tour' => $id
        );

		$this->m_master->model_edit_jenis_tour($where,$data,'tb_jenis_tour');
		$this->db->trans_complete();
        if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Tour Jenis " . $nama_tour . " Berhasil Diedit" );
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('common/JenisTour');
    }

    function delete_jenis_tour(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id_jenis_tour' => $id);
		$this->m_master->model_delete_jenis_tour($where,'tb_jenis_tour');
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('common/JenisTour');
    }
}