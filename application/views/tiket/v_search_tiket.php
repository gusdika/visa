<? $header;
   $sidebar;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
      <div class="row mb-2">
          <div class="col-sm-3">
            <h5 class="m-0 text-dark"><?php echo $header_title?></h5>
          </div>
          <div class="col-sm-4 col-lg-4">
              <form class="form-inline mr-4" action="<?php echo base_url(). 'tiket/ListTiket/search_tiket'; ?>" method="post">
                <div class="input-group input-group-sm">
                  <input class="form-control form-control-navbar" type="search" name="word" placeholder="Search nama, no paspor" aria-label="Search">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </form>
          </div>
          <div class="col-sm-3 col-lg-3">
              <form class="form-inline mr-4" action="<?php echo base_url(). 'tiket/ListTiket/search_by'; ?>" method="post">
              <div class="input-group input-group-sm">
                <div class="dropdown" style="display:flex">
                        <select class="form-control form-control-navbar" name="sort_by" style="height:33px;width: 170px;" required autocomplete="off">
                                <option selected>Search by</option>
                                <option value="1">Lunas</option>
                                <option value="0">Nunggak</option>
                        </select>
                    <div class="input-group-append">
                        <input value="Cari" type="submit">
                    </div>
                  </div>
                </div>
              </form>
          </div>

          <div class="col-sm-2 col-lg-2">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
              <div class="col-lg-3">
              <label>Jadwal Expired</label>
                <form method="post" action="<?php echo base_url(). 'tiket/ListTiket/search_date'; ?>" class="form-inline mr-4">
                <div class="input-group input-group-sm">
                  <input type="date" class="form-control form-control-navbar" name="date_filter">
                  <div class="input-group-append">
                    <input type="submit" value="Filter">
                  </div>
                </div>  
                </form>
              </div>
              <div class="col-lg-6">
              </div>
              <div class="col-lg-3">
              <label>Expired Perbulan</label>
                <form method="post" action="<?php echo base_url(). 'tiket/ListTiket/search_mounth_year'; ?>" class="form-inline mr-4">
                <div class="input-group input-group-sm">
                <select class="form-control" style="width:80px" name="month">
                    <option value="01">Januari</option>
                    <option value="02">Februari</option>
                    <option value="03">Maret</option>
                    <option value="04">April</option>
                    <option value="05">Mei</option>
                    <option value="06">Juni</option>
                    <option value="07">Juli</option>
                    <option value="08">Agustus</option>
                    <option value="09">September</option>
                    <option value="10">Oktober</option>
                    <option value="12">November</option>
                    <option value="12">Desember</option>
                </select>
                <input type="text" class="form-control" style="width:100px" placeholder="Tahun" name="year">
                  <div class="input-group-append">
                    <input type="submit" value="Filter">
                  </div>
                </div>  
                </form>
              </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="list-visa">
            <button class="btn btn-default export-button" id="oke" type="button" >Export CSV</button>
            <div class="table-responsive">
                <table class="table table-bordered">
                <thead>
                <tr>
                <th scope="col">No</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Maskapai</th>
                    <th scope="col">Negara</th>
                    <th scope="col">No Paspor</th>
                    <th scope="col">TTL</th>
                    <th scope="col">Expired</th>
                    <th scope="col">Rute</th>
                    <th scope="col">No Penerbangan</th>
                    <th scope="col">Waktu Destinasi</th>
                    <th scope="col">Tiket Dewasa</th>
                    <th scope="col">Tiket nak</th>
                    <th scope="col">Harga Pertiket</th>
                    <th scope="col">Total</th>
                    <th scope="col">DP</th>
                    <th scope="col">Sisa</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                if(!empty($hasil_search)){
                $no = 1;
                foreach($hasil_search as $data){ 
                ?>
               <tr>
                <input type="text" hidden="" value="<?php echo $data->id_tiket;?>" name="id"></input>
                <th scope="row"><?echo $no?></th>
                <td><?php echo $data->nama?></td>
                    <td><?php echo  $data->nama_maskapai?></td>
                    <td><?php echo  $data->country_name?></td>
                    <td><?php echo  $data->no_paspor?></td>
                    <td><?php echo  $data->t_lahir?></td>
                    <td><?php echo  $data->t_expired?></td>
                    <td><?php echo  $data->rute?></td>
                    <td><?php echo  $data->no_penerbangan?></td>
                    <td><?php echo  $data->time_destinasi?></td>
                    <td><?php echo  $data->tiket_dewasa?></td>
                    <td><?php echo  $data->tiket_anak?></td>
                    <td><?php echo  $data->per_tiket?></td>
                    <td><?php echo  $data->total?></td>
                    <td><?php echo  $data->dp?></td>
                    <td><?php echo  $data->sisa?></td>
                    <td><?php if($data->status !=0) {?>
                        <p style="color:green; font-weight:bold">LUNAS</p>
                        <?} else { ?>
                        <p style="color:red; font-weight:bold">NUNGGAK</p>
                        <?}?>
                    </td>
                    <td>
                    <div class="button-aksi-container">
                        <button type="button" class="btn btn-info btn-aksi" data-toggle="modal" data-target="#modal-edit<?=$data->id_tiket;?>"><i class="far fa-edit"></i></button>
                        <button type="submit" class="btn btn-danger btn-aksi" data-toggle="modal" data-target="#modal_hapus<?php echo $data->id_tiket;?>"><i class="fas fa-trash-alt"></i></button>
                    </div>
                    </td>

                </tr>
                <tr>
                  <? $no++; }

                  } else {?>
                  <caption><p>Data tidak ada</p></caption>
                  <?}?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
</div>
<?php if(isset($hasil_search)){ ?>
<?php $no=0; foreach($hasil_search as $row): $no++; ?>
<div class="modal fade" id="modal-edit<?=$row->id_tiket;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Visa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?php echo base_url(). 'tiket/ListTiket/edit_tiket'; ?>" method="post">
                          <div class="row">
                            <input type="hidden" class="form-control" name="id" value="<?php echo $row->id_tiket?>"  />
                                  <div class="form-group col-md-6">
                                      <div style="display:flex">
                                        <label for="email">Nama</label>
                                        <div class="text-danger">*</div>
                                      </div>
                                      <input type="text" class="form-control" value="<?php echo $row->nama?>" name="nama" placeholder="Masukan nama">
                                  </div>
                                  <div class="form-group col-md-6">
                                      <div style="display:flex">
                                        <label for="pwd">Jenis Maskapai</label>
                                        <div class="text-danger">*</div>
                                      </div>
                                        <div class="dropdown">
                                            <select class="form-control" name="id_maskapai" required autocomplete="off">
                                                    <option value="<?php echo $row->id_maskapai ?>" selected><?php echo $row->nama_maskapai ?></option>
                                                <?php foreach($jenis_maskapai as $result){?>
                                                    <option value="<?php echo $result->id?>"><?php echo $result->nama_maskapai?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                  </div>
                                
                          </div>
                          <div class="row">
                                  <div class="form-group col-md-6">
                                    <div style="display:flex">
                                        <label for="email">No Paspor</label>
                                        <div class="text-danger">*</div>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $row->no_paspor ?>"  name="no_paspor" placeholder="Enter no visa" id="email">
                                  </div>
                                  <div class="form-group col-md-6">
                                  <div style="display:flex">
                                      <label for="pwd">Negara</label>
                                    </div>
                                    <div class="dropdown">
                                                <select class="form-control" name="id_negara" required autocomplete="off">
                                                    <option value="<?php echo $row->id_negara?>" selected><?php echo $row->country_name?></option>
                                                <?php foreach($negara as $result){?>
                                                    <option value="<?php echo $result->id?>"><?php echo  $result->country_name?></option>
                                                <? } ?>
                                                </select>
                                    </div>
                                </div>
                            <div class="form-group col-md-6">
                                <label for="birthday">Tanggal Lahir</label>
                                <input type="date" name="t_lahir" value="<?php echo $row->t_lahir?>" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                              <label for="birthday">Expired</label>
                                    <input type="date" name="t_expired" value="<?php echo $row->t_expired?>" class="form-control">
                              </div>
                          </div>

                          <div class="row">
                                  <div class="form-group col-md-6">
                                    <label for="email">Rute</label>
                                    <input type="text" class="form-control" name="rute" value="<?php echo $row->rute?>" placeholder="rute">
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="pwd">No Penerbangan</label>
                                    <input type="number" class="form-control" name="no_penerbangan" value="<?php echo $row->no_penerbangan?>" placeholder="no penerbangan">
                                  </div>
                                
                          </div>
                          <div class="row hitung-modal">
                              <div class="form-group col-md-6">
                              <label for="birthday">Time Destination</label>
                                    <input type="text" class="form-control" name="time_destination" value="<?php echo $row->time_destinasi?>" placeholder="Waktu yang ditempuh">
                              </div>
                              <div class="form-group col-md-6">
                                <label for="birthday">Dewasa</label>
                                      <input type="text" class="form-control" name="tiket_dewasa" value="<?php echo $row->tiket_dewasa?>" placeholder="Jumlah Tiket Dewasa" id="jum_dewasa-modal<?php echo $no?>">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Anak</label>
                                    <input type="text" class="form-control" name="tiket_anak" value="<?php echo $row->tiket_anak?>" placeholder="Jumlah Tiket Anak" id="jum_anak-modal<?php echo $no?>">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Biaya Pertiket</label>
                                    <input type="text" class="per_tiket form-control" name="per_tiket" value="<?php echo $row->per_tiket?>" placeholder="Total" id="per_tiket-modal<?php echo $no?>">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">DP</label>
                                    <input type="text" class="dp form-control" name="dp" value="<?php echo $row->dp?>" placeholder="dp" id="dp-modal<?php echo $no?>">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Total Biaya</label>
                                    <input type="text" class="total form-control" name="total" value="<?php echo $row->total?>" placeholder="Total" id="total-modal<?php echo $no?>" readonly="readonly">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Sisa</label>
                                    <input type="text" class="sisa form-control" name="sisa" value="<?php echo $row->sisa?>" placeholder="Sisa" id="sisa-modal<?php echo $no?>" readonly="readonly">
                              </div>
                          </div>
                          
                <div class="row">
                        <div class="form-group col-md-6">
                        <label for="pwd">Status</label>
                        <label class="switch">
                        <?php if($row->status == 1){?>
                          <input type="checkbox" name="status[]" checked>
                          <span class="slider round"></span>
                       <? }else { ?>
                        <input type="checkbox" name="status[]">
                        <span class="slider round"></span>
                        <? }?>
                        </label>
                      </div>
                </div>
                
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </form>
      </div>

    </div>
  </div>
</div>
<?php endforeach; ?>
<? } ?>

<?php if(isset($hasil_search)){ ?>

<?php $no=0; foreach($hasil_search as $row): $no++; ?>
<div class="modal fade" id="modal_hapus<?php echo $row->id_tiket;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'tiket/ListTiket/delete_tiket'; ?>">
            <input type="hidden" class="form-control" name="id" value="<?php echo $row->id_tiket;?>"  />

                <div class="modal-body">
                    <p>Anda yakin mau menghapus <b><?php echo $row->nama;?></b></p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach; ?>
<?}?>
<? $footer;
   $script;
?>

<script type="text/javascript">

$( '.per_tiket' ).mask('0.000.000.000', {reverse: true});

$( '.dp' ).mask('0.000.000.000', {reverse: true});

$( '.sisa' ).mask('0.000.000.000', {reverse: true});


$(document).ready(function(){
  let numberSelected = 10;
  for (let i = 1; i < numberSelected; i++) {

    $('.hitung-modal').keyup(function(){
      var anak = $("#jum_anak-modal"+i).val()
      var dewasa = $("#jum_dewasa-modal"+i).val()
      var pertiket = $("#per_tiket-modal"+i).val()
			var dp = $("#dp-modal"+i).val()
      var x =  pertiket.replace(/\./g,'');
			var y =  dp.replace(/\./g,'');
      if(anak == 1 || dewasa == 1){
        var total = (anak * x) + (dewasa * x);
      }else if(anak == 0){
        var total = x * dewasa;
      }else if(dewasa == 0){
        var total = x * anak;
      }else if(dewasa == 0 && anak){
        var total = x;
      }else{
        var total = x * dewasa * anak;
      }
			var sisa = total - y;
      $('#total-modal'+i).val(total); 
      $('#sisa-modal'+i).val(sisa); 
    });
  
  
  }

  })


var data = [];
      var titlehead = ['No','Nama','Maskapai','Negara','No Paspor','Tanggal Lahir','Expired', 'Rute','No Penerbangan','Waktu Destinasi','Tiket Dewasa','Tiket Anak','Harga Pertiket','Total Biaya','Total Bayar','Sisa','Status'];
      data.push(titlehead);
      '<?php  $no = 1; foreach($hasil_search as $data){?> ';
        '<?php if($data->status != 0){
          $status = 'LUNAS';
        }else{
          $status = 'BELUM LUNAS';
        }?>'
        var temp = ['<?php echo $no?>','<?php echo $data->nama?>', '<?php echo  $data->nama_maskapai?>', '<?php echo  $data->country_name?>', '<?php echo  $data->no_paspor?>', '<?php echo  $data->t_lahir?>', '<?php echo  $data->t_expired?>', '<?php echo  $data->rute?>','<?php echo  $data->no_penerbangan?>','<?php echo  $data->time_destinasi?>','<?php echo  $data->tiket_dewasa?>','<?php echo  $data->tiket_anak?>','<?php echo  number_format($data->per_tiket,2,'.','.')?>','<?php echo  number_format($data->total,2,'.','.')?>','<?php echo  number_format($data->dp,2,'.','.')?>','<?php echo  number_format($data->sisa,2,'.','.')?>','<?php echo $status?>']; 
        data.push(temp);
      '<? $no++; }?>'


    $('#oke').on('click',function(){
      var csvContent = '';
      data.forEach(function(infoArray, index) {
      dataString = infoArray.join(';');
      csvContent += index < data.length ? dataString + '\n' : dataString;
    });

    var download = function(content, fileName, mimeType) {
      var a = document.createElement('a');
      mimeType = mimeType || 'application/octet-stream';

      if (navigator.msSaveBlob) { // IE10
        navigator.msSaveBlob(new Blob([content], {
          type: mimeType
        }), fileName);
      } else if (URL && 'download' in a) { //html5 A[download]
        a.href = URL.createObjectURL(new Blob([content], {
          type: mimeType
        }));
        a.setAttribute('download', fileName);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      } else {
        location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
      }
    }

    download(csvContent, 'Tiket.csv', 'text/csv;encoding:utf-8');

    });


$(document).ready(function(){

// Format mata uang.
$( '.total' ).mask('0.000.000.000', {reverse: true});

// Format nomor HP.
$( '.dp' ).mask('0.000.000.000', {reverse: true});

// Format tahun pelajaran.
$( '.sisa' ).mask('0.000.000.000', {reverse: true});
})

$(".perhitungan").keyup(function(){
			var total = $("#total").val()
			var bayar = $("#bayar").val()
			var x =  total.replace(/\./g,'');
			var y =  bayar.replace(/\./g,'');
			var sisa = x - y;
			$("#sisa").attr("value",sisa);
    });

</script>