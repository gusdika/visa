<?php 
 
class M_tiket extends CI_Model{

    function get_master_jenis_maskapai(){
        $data = $this->db->query('SELECT * FROM `tb_maskapai` WHERE 1');
        return $data;
    }
     
	function model_add_tiket($data,$table){
		$this->db->insert($table,$data);
    }

    function get_list_tiket_last(){
        $data = $this->db->query('SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id ORDER BY tv.id_tiket DESC LIMIT 1');
        return $data;
      }
      function get_list_tiket_all(){
        $data = $this->db->query('SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id ORDER BY tv.id_tiket WHERE 1');
        return $data;
      }
    
      function get_list_tiket(){
        $data = $this->db->query('SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id ORDER BY tv.id_tiket DESC');
        return $data;
      }
    function model_edit_tiket($where,$data,$table){
      $this->db->where($where);
      $this->db->update($table,$data);
    }
    function model_delete_tiket($where,$table){
          $this->db->where($where);
          $this->db->delete($table);
    }

    function model_search_by($data){
      $search = $this->db->query('SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE tv.status = ' . $data . '');
        return $search;
    }
    function model_search_date($data){
      $search = $this->db->query("SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE tv.t_expired = '". $data . "'");
        return $search;
    }
    function model_search_month_year($month, $year){
      $search = $this->db->query("SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE MONTH(t_expired) = '" . $month . "' AND YEAR(t_expired) = '" . $year . "'");
        return $search;
    }
    function model_search_tiket($data) {
      $search = $this->db->query('SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE no_paspor LIKE "%' . $data . '%" OR nama LIKE "%' . $data . '%"');
      return $search;
  }
  function get_list_tiket_pdf($id) {
    $search = $this->db->query('SELECT * FROM `tb_tiket` tv LEFT JOIN tb_maskapai tj ON tv.id_maskapai = tj.id LEFT JOIN apps_countries ap ON tv.id_negara = ap.id WHERE tv.id_tiket = ' . $id . '');
    return $search;
  }

}

