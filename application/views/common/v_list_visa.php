<? $header;
   $sidebar;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-3">
            <h5 class="m-0 text-dark"><?php echo $header_title?></h5>
          </div>
          <div class="col-sm-4 col-lg-4">
              <form class="form-inline mr-4" action="<?php echo base_url(). 'dashboard/search_visa'; ?>" method="post">
                <div class="input-group input-group-sm">
                  <input class="form-control form-control-navbar" type="search" name="word" placeholder="Search nama, no paspor" aria-label="Search">
                  <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                  </div>
                </div>
              </form>
          </div>
          <div class="col-sm-2 col-lg-2">
              <form class="form-inline mr-4" action="<?php echo base_url(). 'dashboard/search_by'; ?>" method="post">
              <div class="input-group input-group-sm">
                <div class="dropdown" style="display:flex">
                        <select class="form-control form-control-navbar" name="sort_by" style="height:33px;width: 170px;" required autocomplete="off">
                                <option selected>Search by</option>
                                <option value="1">Lunas</option>
                                <option value="0">Nunggak</option>
                        </select>
                    <div class="input-group-append">
                        <input value="Cari" type="submit">
                    </div>
                  </div>
                </div>
              </form>
          </div>

          <div class="col-sm-3">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row">
              <div class="col-lg-3">
              <label>Jadwal foto</label>
                <form method="post" action="<?php echo base_url(). 'dashboard/search_date'; ?>" class="form-inline mr-4">
                <div class="input-group input-group-sm">
                  <input type="date" class="form-control form-control-navbar" name="date_filter">
                  <div class="input-group-append">
                    <input type="submit" value="Filter">
                  </div>
                </div>  
                </form>
              </div>
              <div class="col-lg-6">
              </div>
              <div class="col-lg-3">
              <label>Transaksi Perbulan</label>
                <form method="post" action="<?php echo base_url(). 'dashboard/search_mounth_year'; ?>" class="form-inline mr-4">
                <div class="input-group input-group-sm">
                  <select class="form-control" style="width:80px" name="month">
                      <option value="01">Januari</option>
                      <option value="02">Februari</option>
                      <option value="03">Maret</option>
                      <option value="04">April</option>
                      <option value="05">Mei</option>
                      <option value="06">Juni</option>
                      <option value="07">Juli</option>
                      <option value="08">Agustus</option>
                      <option value="09">September</option>
                      <option value="10">Oktober</option>
                      <option value="12">November</option>
                      <option value="12">Desember</option>
                  </select>
                <input type="text" class="form-control" style="width:100px" placeholder="Tahun" name="year">
                  <div class="input-group-append">
                    <input type="submit" value="Filter">
                  </div>
                </div>  
                </form>
              </div>
        </div>
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php if (!empty($this->session->flashdata('error'))){?>
                            <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('error');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
    <?} else if(!empty($this->session->flashdata('success'))) {?>
      <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success');?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?}?>
            <div id="list-visa">
                <button class="btn btn-default export-button" id="oke" type="button" >Export CSV</button>
            <div class="table-responsive">
                <table class="table table-bordered">
                <thead>
                <tr>
                    <th scope="col" >No</th>
                    <th scope="col" >Name</th>
                    <th scope="col" >No Paspor</th>
                    <th scope="col" >Jenis Visa</th>
                    <th scope="col" >Email</th>
                    <th scope="col" >Negara</th>
                    <th scope="col" >Exp 1</th>
                    <th scope="col" >Exp 2</th>
                    <th scope="col" >Jadwal Foto</th>
                    <th scope="col" >Alamat</th>
                    <th scope="col" >Phone</th>
                    <th scope="col" >Transaksi</th>
                    <th scope="col" >Total</th>
                    <th scope="col" >DP</th>
                    <th scope="col" >Sisa</th>
                    <th scope="col" >Keterangan</th>
                    <th scope="col" >Status</th>
                    <th scope="col" >Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                $no = 1;
                foreach($list_visa_all as $data){ 
                ?>
                <tr>
                <input type="text" hidden="" value="<?php echo $data->id_visa;?>" name="id"></input>
                <th scope="row"><?echo $no?></th>
                  <td><?php echo $data->nama?></td>
                  <td><?php echo  $data->no_paspor?></td>
                  <td><?php echo  $data->nama_visa?></td>
                  <td><?php echo  $data->email?></td>
                  <td><?php echo  $data->country_name?></td>
                  <td><?php echo  $data->expired_v1?></td>
                  <td><?php echo  $data->expired_v2?></td>
                  <td><?php echo  $data->jadwal_foto?></td>
                  <td><?php echo  $data->alamat?></td>
                  <td><?php echo  $data->telp?></td>
                  <td><?php echo  $data->tgl_transaksi?></td>
                  <td><?php echo  number_format($data->total_biaya,2,'.','.')?></td>
                  <td><?php echo  number_format($data->dp,2,'.','.')?></td>
                  <td><?php echo  number_format($data->sisa,2,'.','.')?></td>
                  <td><?php echo  $data->keterangan?></td>
                    <td><?php if($data->status !=0) {?>
                        <p style="color:green; font-weight:bold">LUNAS</p>
                        <?} else { ?>
                        <p style="color:red; font-weight:bold">BELUM LUNAS</p>
                        <?}?>
                    </td>
                    <td>
                    <div class="button-aksi-container">
                        <button type="button" class="btn btn-info btn-aksi" data-toggle="modal" data-target="#modal-edit<?=$data->id_visa;?>"><i class="far fa-edit"></i></button>
                        <button type="submit" class="btn btn-danger btn-aksi" data-toggle="modal" data-target="#modal_hapus<?php echo $data->id_visa;?>"><i class="fas fa-trash-alt"></i></button>
                        <form action="<?php echo base_url(). 'common/ListVisa/pdf'; ?>" method="post">
                            <input type="hidden" class="form-control" name="id" value="<?php echo $data->id_visa;?>"  />
                            <button type="submit" class="btn btn-warning btn-aksi" data-toggle="modal"><i class="fas fa-file"></i></button>
                        </form>
                    </div>
                    </td>

                </tr>
                <tr>
                <? $no++; }?>
                </tbody>
                </table>
            </div>
            </div>
        </div>
</div>
<?php $no=0; foreach($list_visa_all as $row): $no++; ?>
<div class="modal fade" id="modal-edit<?=$row->id_visa;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Visa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?php echo base_url(). 'common/ListVisa/edit_visa'; ?>" method="post">
                                <div class="row">
                                  <input type="hidden" class="form-control" name="id" value="<?php echo $row->id_visa;?>"  />
                                    <div class="form-group col-md-6">
                                          <div style="display:flex">
                                              <label for="email">Name</label>
                                              <div class="text-danger">*</div>
                                          </div>
                                            <input type="text" class="form-control" name="nama" value="<?php echo $row->nama;?>" placeholder="Enter name" id="email">
                                          </div>
                                          <div class="form-group col-md-6">
                                            <div style="display:flex">
                                              <label for="pwd">Jenis Visa</label>
                                              <div class="text-danger">*</div>
                                           </div>
                                              <div class="dropdown">
                                                    <select class="form-control" name="jenis_visa" required autocomplete="off">
                                                            <option value="<?php echo $row->id_jenis_visa?>" selected><?php echo $row->nama_visa?></option>
                                                        <?php foreach($jenis_visa as $result){?>
                                                            <option value="<?php echo $result->id?>"><?php echo $result->nama_visa?></option>
                                                        <? } ?>
                                                    </select>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                    <label for="email">No Paspor</label>
                                                    <div class="text-danger">*</div>
                                                </div>
                                                <input type="text" class="form-control" name="no_paspor" value="<?php echo $row->no_paspor;?>" placeholder="Enter no visa" id="email">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="pwd">Email</label>
                                                <input type="text" class="form-control" name="email" value="<?php echo $row->email;?>" placeholder="Enter work" id="pwd">
                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Expired Visa 1</label>
                                            <input type="date" name="expired_v1" value="<?php echo $row->expired_v1;?>" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                          <label for="birthday">Expired Visa 2</label>
                                                <input type="date" name="expired_v2" value="<?php echo $row->expired_v2;?>" class="form-control">
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Jdwal Foto</label>
                                            <input type="date" name="jadwal_foto" value="<?php echo $row->jadwal_foto;?>" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                              <div style="display:flex">
                                                  <label for="pwd">Negara</label>
                                                </div>
                                                <div class="dropdown">
                                                            <select class="form-control" name="negara" required autocomplete="off">
                                                                <option value="<?php echo $row->id?>" selected><?php echo $row->country_name?></option>
                                                            <?php foreach($negara as $result){?>
                                                                <option value="<?php echo $result->id?>"><?php echo $result->country_name?></option>
                                                            <? } ?>
                                                            </select>
                                                </div>
                                            </div>
                                      </div>

                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <label for="email">Keterangan</label>
                                                <input type="text" class="form-control" name="keterangan" value="<?php echo $row->keterangan;?>" placeholder="Enter nik" id="email"></textarea>
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="pwd">Phone/WA</label>
                                                <input type="number" class="form-control" name="telp" value="<?php echo $row->telp;?>" placeholder="Enter tlp" id="pwd">
                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                              <div class="form-group col-md-12">
                                                <label for="email">Alamat</label>
                                                <textarea type="text" class="form-control" name="alamat" placeholder="Enter address" id="email"><?php echo $row->alamat;?></textarea>
                                              </div>
                                      </div>
                                     
                                      <div class="row calchangelist">
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Tanggal Transaksi</label>
                                            <input type="date" name="tgl_transaksi" value="<?php echo $row->tgl_transaksi;?>" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                          <label for="birthday">Total Biaya</label>
                                                <input type="text" class="total form-control" name="total_biaya" value="<?php echo $row->total_biaya?>" placeholder="Total" id="total-modal-list<?php echo $no?>">
                                          </div>
                                        <div class="form-group col-md-6">
                                          <label for="birthday">DP</label>
                                                <input type="text" class="dp form-control" name="dp" value="<?php echo $row->dp?>" placeholder="DP" id="bayar-modal-list<?php echo $no?>">
                                        </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">Sisa</label>
                                                <input type="text" class="sisa form-control" name="sisa" value="<?php echo $row->sisa?>" placeholder="Sisa" id="sisa-modal-list<?php echo $no?>" readonly="readonly">
                                          </div>
                                    </div>
                          
                <div class="row">
                        <div class="form-group col-md-6">
                        <label for="pwd">Status</label>
                        <label class="switch">
                        <?php if($row->status == 1){?>
                          <input type="checkbox" name="status[]" checked>
                          <span class="slider round"></span>
                       <? }else { ?>
                        <input type="checkbox" name="status[]">
                        <span class="slider round"></span>
                        <? }?>
                        </label>
                      </div>
                </div>
                
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-primary" name="update" value="Submit" >
                </div>
        </form>
      </div>

    </div>
  </div>
</div>
<?php endforeach; ?>


<?php $no=0; foreach($list_visa_all as $row): $no++; ?>
<div class="modal fade" id="modal_hapus<?php echo $row->id_visa;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'common/ListVisa/delete_visa'; ?>">
            <input type="hidden" class="form-control" name="id" value="<?php echo $row->id_visa;?>"  />

                <div class="modal-body">
                    <p>Anda yakin mau menghapus <b><?php echo $row->nama;?></b></p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach; ?>

<? $footer;
   $script;
?>

<script type="text/javascript">
$( '.total' ).mask('0.000.000.000', {reverse: true});

$( '.dp' ).mask('0.000.000.000', {reverse: true});

$( '.sisa' ).mask('0.000.000.000', {reverse: true});


$(document).ready(function(){
  let numberSelected = 10;
  for (let i = 1; i < numberSelected; i++) {
  $('.calchangelist').keyup(function(){
      var total = $("#total-modal-list"+i).val()
			var bayar = $("#bayar-modal-list"+i).val()
			var x =  total.replace(/\./g,'');
			var y =  bayar.replace(/\./g,'');
      console.log(x);
      console.log(y);
			var sisa = x - y;
      console.log(sisa);
			$("#sisa-modal-list"+i).val(sisa); 

    });

  }

  })

  var data = [];
      var titlehead = ['No','Nama','No Paspor','Nama Visa','Email','Negara','Tanggal Expired 1', 'Tanggal Expired 2','Jadwal Foto','Alamat','No Telpon','Tanggal Transaksi','Total Biaya','Total Bayar','Sisa','Keterangan','Status'];
      data.push(titlehead);
      '<?php  $no = 1; foreach($list_visa_all as $data){?> ';
        '<?php if($data->status != 0){
          $status = 'LUNAS';
        }else{
          $status = 'BELUM LUNAS';
        }?>'
        var temp = ['<?php echo $no?>','<?php echo $data->nama?>', '<?php echo  $data->no_paspor?>', '<?php echo  $data->nama_visa?>', '<?php echo  $data->email?>', '<?php echo  $data->country_name?>', '<?php echo  $data->expired_v1?>', '<?php echo  $data->expired_v2?>','<?php echo  $data->jadwal_foto?>','<?php echo  $data->alamat?>','<?php echo  $data->telp?>','<?php echo  $data->tgl_transaksi?>','<?php echo  number_format($data->total_biaya,2,'.','.')?>','<?php echo  number_format($data->dp,2,'.','.')?>','<?php echo  number_format($data->sisa,2,'.','.')?>','<?php echo  $data->keterangan?>','<?php echo $status?>']; 
        data.push(temp);
      '<? $no++; }?>'


    $('#oke').on('click',function(){
      var csvContent = '';
      data.forEach(function(infoArray, index) {
      dataString = infoArray.join(';');
      csvContent += index < data.length ? dataString + '\n' : dataString;
    });

    var download = function(content, fileName, mimeType) {
      var a = document.createElement('a');
      mimeType = mimeType || 'application/octet-stream';

      if (navigator.msSaveBlob) { // IE10
        navigator.msSaveBlob(new Blob([content], {
          type: mimeType
        }), fileName);
      } else if (URL && 'download' in a) { //html5 A[download]
        a.href = URL.createObjectURL(new Blob([content], {
          type: mimeType
        }));
        a.setAttribute('download', fileName);
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
      } else {
        location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
      }
    }

    download(csvContent, 'Visa.csv', 'text/csv;encoding:utf-8');

    });
</script>