<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisVisa extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_master');
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Master Jenis Visa';
		$data['breadchumb'] = 'Input Master Visa'; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['list_master_jenis_visa'] = $this->m_master->get_list_master_jenis_visa()->result();

        $this->load->view('common/v_master_jenis_visa', $data);
    }

    public function add_jenis_visa(){
		if(!empty($this->input->post('nama') || $this->input->post('keterangan'))){
			$nama = $this->input->post('nama');
            $keterangan = $this->input->post('keterangan');
			$data = array(
				'nama_visa' => $nama,
				'k_jenis_visa' => $keterangan,
				);

			$res = $this->m_master->model_add_jenis_visa($data,'tb_jenis_visa');
			$this->session->set_flashdata('success', "Visa Berhasil Didaftarkan"); 
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('common/JenisVisa');
    }

    public function edit_jenis_visa(){
        $this->db->trans_start();
		$id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $keterangan = $this->input->post('keterangan');

        $data = array(
            'nama_visa' => $nama,
            'k_jenis_visa' => $keterangan,
            );
        
        $where = array(
            'id' => $id
        );
		$this->m_master->model_edit_jenis_visa($where,$data,'tb_jenis_visa');
		$this->db->trans_complete();
        if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Visa Jenis " . $nama . " Berhasil Diedit" );
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('common/JenisVisa');
    }

    function delete_jenis_visa(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id' => $id);
		$this->m_master->model_delete_jenis_visa($where,'tb_jenis_visa');
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('common/JenisVisa');
    }
}