<? $header;
   $sidebar;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $header_title?></h1>
          </div>

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="<?echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php if (!empty($this->session->flashdata('error'))){?>
                            <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('error');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
    <?} else if(!empty($this->session->flashdata('success'))) {?>
      <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success');?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
      </div>
    <?}?>
            <div id="list-visa">
                <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                <th scope="col">No</th>
                                <th scope="col">Jumlah</th>
                                <th scope="col">id_visa</th>
                                <th scope="col">id_tiket</th>
                                <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach($list_kas_masuk as $data){ 
                            ?>
                            <tr>
                            <input type="text" hidden="" value="<?php echo $data->id_tiket;?>" name="id"></input>
                            <th scope="row"><?echo $no?></th>
                                <td><?php echo $data->jum?></td>
                                <td><?php echo  $data->id_visa?></td>
                                <td><?php echo  $data->id_tiket?></td>
                                <td>
                                    <div class="button-aksi-container">
                                    <button type="submit" class="btn btn-warning btn-aksi" data-toggle="modal"><i class="fas fa-file"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                            <? $no++; }?>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>

<? $footer;
   $script;
?>