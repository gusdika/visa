<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InputVisa extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_dashboard');
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Input Visa';
		$data['breadchumb'] = 'Input Visa'; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['list_visa_last'] = $this->m_dashboard->get_list_visa_last()->result();
        $data['jenis_visa'] = $this->m_dashboard->get_jenis_visa()->result();
        $data['negara'] = $this->m_dashboard->get_negara()->result();
        
        $this->load->view('common/v_input_visa', $data);
    }

    public function add_visa(){
		if(!empty($this->input->post('jenis_visa') && $this->input->post('nama') && $this->input->post('no_paspor') && $this->input->post('expired_v1') && $this->input->post('expired_v2') && $this->input->post('jadwal_foto') )){
			$nama = $this->input->post('nama');
			$id_jenis_visa = $this->input->post('jenis_visa');
			$no_paspor = $this->input->post('no_paspor');
			$email = $this->input->post('email');
			$expired_v1 = $this->input->post('expired_v1');
            $expired_v2 = $this->input->post('expired_v2');
			$jadwal_foto = $this->input->post('jadwal_foto');
			$keterangan = $this->input->post('keterangan');
			$alamat = $this->input->post('alamat');
			$telp = $this->input->post('telp');
            $negara = $this->input->post('negara');
            $tgl_transaksi = $this->input->post('tgl_transaksi');
            $total_biaya = str_replace(".","",strtolower(trim($this->input->post('total_biaya'))));
            $dp = str_replace(".","",strtolower(trim($this->input->post('dp'))));
            $sisa = $this->input->post('sisa');
			if($total_biaya <= $dp) {
				$status = 1;
			} else {
				$status = 0;
			}
			$data = array(
				'nama' => $nama,
				'id_jenis_visa' => $id_jenis_visa,
				'no_paspor' => $no_paspor,
				'email' => $email,
				'expired_v1' => $expired_v1,
                'expired_v2' => $expired_v2,
                'jadwal_foto' => $jadwal_foto,
				'keterangan' => $keterangan,
                'alamat' => $alamat,
                'telp'  => $telp,
                'id_negara' => $negara,
                'tgl_transaksi' => $tgl_transaksi,
				'total_biaya' => $total_biaya,
				'dp' => $dp,
				'sisa' => str_replace(".","",strtolower(trim($sisa))),
				'status' => $status

				);
			$res = $this->m_dashboard->model_add_visa($data,'tb_visa');
			$this->session->set_flashdata('success', "Visa Berhasil Didaftarkan"); 
			$data_last = $this->m_dashboard->get_list_visa_last()->result();
			$data_last_view= json_decode( json_encode($data_last), true);
			$array = json_decode( json_encode($data_last), true);
			foreach ($array as $result) {
				$income = array(
					'jum' => $result['dp'],
					'id_visa' => $result['id_visa']
				);
				$masuk_kas = $this->m_dashboard->add_kas_masuk($income,'tb_income');
			}


			$this->session->set_flashdata('last_added',$data_last_view);
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('common/InputVisa');
	}

	function edit_visa(){
		$this->db->trans_start();
		$id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $id_jenis_visa = $this->input->post('jenis_visa');
        $no_paspor = $this->input->post('no_paspor');
        $email = $this->input->post('email');
        $expired_v1 = $this->input->post('expired_v1');
        $expired_v2 = $this->input->post('expired_v2');
        $jadwal_foto = $this->input->post('jadwal_foto');
        $keterangan = $this->input->post('keterangan');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $negara = $this->input->post('negara');
        $tgl_transaksi = $this->input->post('tgl_transaksi');
		$total_biaya = str_replace(".","",strtolower(trim($this->input->post('total_biaya'))));
		$dp = str_replace(".","",strtolower(trim($this->input->post('dp'))));
        $sisa = $this->input->post('sisa');
		$status = $this->input->post('status');
		if($total_biaya <= $dp) {
			$status = 1;
		} else {
			$status = 0;
		}

		$data = array(
            'nama' => $nama,
            'id_jenis_visa' => $id_jenis_visa,
            'no_paspor' => $no_paspor,
            'email' => $email,
            'expired_v1' => $expired_v1,
            'expired_v2' => $expired_v2,
            'jadwal_foto' => $jadwal_foto,
            'keterangan' => $keterangan,
            'alamat' => $alamat,
            'telp'  => $telp,
            'id_negara' => $negara,
            'tgl_transaksi' => $tgl_transaksi,
			'total_biaya' => $total_biaya,
			'dp' => $dp,
			'sisa' => str_replace(".","",strtolower(trim($sisa))),
			'status' => $status
		);
		$where = array(
			'id_visa' => $id
		);

		$this->m_dashboard->model_edit_visa($where,$data, 'tb_visa');
		$this->m_dashboard->model_edit_kas_masuk($id, $dp);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Visa Dengan No " . $no_paspor . " Berhasil Diedit" );
			$data_last = $this->m_dashboard->get_list_visa_last()->result();
			$data_last_view= json_decode( json_encode($data_last), true);
			$this->session->set_flashdata('last_added',$data_last_view);
            
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('common/InputVisa');

	}

	function delete_visa(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id_visa' => $id);
		$this->m_dashboard->model_delete_visa($where,'tb_visa');
		$this->m_dashboard->modal_delete_kas_masuk($id);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('common/InputVisa');
    }
    
    public function getListVisaLast(){
		$data['list_visa_last'] = $this->m_dashboard->get_list_visa_last()->result();
		return $data;
	}
}