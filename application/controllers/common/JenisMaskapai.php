<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisMaskapai extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_master');
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Master Jenis Maskapai';
		$data['breadchumb'] = 'Master Maskapai '; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['list_master_jenis_maskapai'] = $this->m_master->get_list_master_jenis_maskapai()->result();

        $this->load->view('common/v_master_jenis_maskapai', $data);
    }

    public function add_jenis_maskapai(){
		if(!empty($this->input->post('nama_maskapai') || $this->input->post('deskripsi'))){
			$nama_maskapai = $this->input->post('nama_maskapai');
            $deskripsi = $this->input->post('deskripsi');
			$data = array(
				'nama_maskapai' => $nama_maskapai,
                'deskripsi' => $deskripsi,
				);

			$res = $this->m_master->model_add_jenis_maskapai($data,'tb_maskapai');
			$this->session->set_flashdata('success', "Maskapai Berhasil Didaftarkan"); 
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('common/JenisMaskapai');
    }

    public function edit_jenis_maskapai(){
        $this->db->trans_start();
		$this->load->helper('url');
		$id = $this->input->post('id');
        $nama_maskapai = $this->input->post('nama_maskapai');
        $deskripsi = $this->input->post('deskripsi');

        $data = array(
            'nama_maskapai' => $nama_maskapai,
            'deskripsi' => $deskripsi,
            );
        
        $where = array(
            'id' => $id
        );

		$this->m_master->model_edit_jenis_maskapai($where,$data,'tb_maskapai');
		$this->db->trans_complete();
        if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Maskapai Jenis " . $nama_maskapai . " Berhasil Diedit" );
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('common/JenisMaskapai');
    }

    function delete_jenis_maskapai(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id' => $id);
		$this->m_master->model_delete_jenis_maskapai($where,'tb_maskapai');
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('common/JenisMaskapai');
    }
}