<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Script extends CI_Controller {

	public function index()
	{
		// ----------------------------
		// format penulisan: array(
		//   'Teks|ikon|alamat_link', 
		// );
		// ----------------------------
		$this->load->view('component/script.php');
		
	}
}
