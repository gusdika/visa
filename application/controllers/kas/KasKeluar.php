<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KasKeluar extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_kas');
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Input Kas Keluar';
		$data['breadchumb'] = 'Kas Keluar'; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
		$data['script'] = $this->load->view('component/script');
		$data['jenis_transaksi'] = $this->m_kas->get_list_master_jenis_transaksi()->result();
        $data['kas_outcome_list'] = $this->m_kas->get_list_kas_keluar()->result();
        $this->load->view('kas/v_kas_keluar', $data);
    }

    public function add_kas_keluar(){
		if(!empty($this->input->post('id_transaksi') && $this->input->post('total'))){
			$id_transaksi = $this->input->post('id_transaksi');
			$total = $this->input->post('total');
			$ket_outcome = $this->input->post('ket_outcome');

			$data = array(
				'id_transaksi' => $id_transaksi,
				'total' => str_replace(".","",strtolower(trim($total))),
				'ket_outcome' => $ket_outcome,

				);

			$this->m_kas->model_add_kas_keluar($data,'tb_outcome');
			$this->session->set_flashdata('success', "Pengeluaran Kas Berhasil Ditambahkan"); 
			
			$this->session->set_flashdata('last_added',$kas_outcome_list);
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('kas/KasKeluar');
	}

	function edit_kas_keluar(){
		$this->db->trans_start();
		$id_outcome = $this->input->post('id_outcome');
		$id_transaksi = $this->input->post('id_transaksi');
		$total = $this->input->post('total');
		$ket_outcome = $this->input->post('ket_outcome');

		$data = array(
			'id_transaksi' => $id_transaksi,
			'total' => str_replace(".","",strtolower(trim($total))),
			'ket_outcome' => $ket_outcome,
		);


		$where = array(
			'id_outcome' => $id_outcome
		);

		$this->m_kas->model_edit_kas_keluar($where,$data,'tb_outcome');
		$this->db->trans_complete();
        if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Kas Keluar Berhasil Diedit" );
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('kas/KasKeluar');
	}

	function delete_kas_keluar(){
		$this->db->trans_start();
		$id_outcome = $this->input->post('id_outcome');
		$where = array('id_outcome' => $id_outcome);
		$this->m_kas->model_delete_kas_keluar($where,'tb_outcome');
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('kas/KasKeluar');
    }
    
    public function getListVisaLast(){
		$data['list_visa_last'] = $this->m_dashboard->get_list_visa_last()->result();
		return $data;
	}
}