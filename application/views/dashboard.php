<? $header;
   $sidebar;
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content
    <section class="content">
      <div class="container-fluid">
        Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <?if(isset($sum_total_masuk)){?>
                <h4><?php echo $sum_total_masuk ?></h4>
                <?php  } else {?>
                <h3>0</h3>
                <?}?>
                <p>Kas Masuk</p>
              </div>
              <div class="icon">
              <i class="ion ion-pie-graph"></i>
              </div>
              <form method="post" action="<?php echo base_url(). 'dashboard/GetListKasMasuk'; ?>">
                <button class="button-get-content small-box-footer">Show Table <i class="fas fa-arrow-circle-right"></i></button>
                </form>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <?if(isset($sum_total_keluar)){?>
                <h4><?php echo $sum_total_keluar ?></h4>
                <?php  } else {?>
                <h3>0</h3>
                <?}?>
                <p>Kas Keluar</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <form method="post" action="<?php echo base_url(). 'dashboard/GetListKasKeluar'; ?>">
                <button class="button-get-content small-box-footer">Show Table <i class="fas fa-arrow-circle-right"></i></button>
              </form>
            </div>
          </div>
           <!-- ./col -->
           <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-dark">
              <div class="inner">
              <?if(isset($selisih)){?>
                <h4><?php echo $selisih ?></h4>
                <?php  } else {?>
                <h3>0</h3>
                <?}?>
                <p>Selisih</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a   class="small-box-footer">Selisih</a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner" style="padding-bottom: 0;">
                <h4><?php echo $total_visa?></h4>

                <p style="margin: 0;">Visa Terdaftar</p>
              </div>
              <div class="inner-flex">
                  <div style="display:flex">
                    <p style="margin: 0;padding-left:10px;font-weight:bold;color:green"><?php echo $total_visa_active?></p><p style="margin: 0;padding-left:2px;font-weight:bold;color:green">LUNAS</p>
                  </div>
                  <p style="margin: 0;padding-left:10px;padding-right:10px;"></p>
                  <div style="display:flex">
                    <p style="margin: 0;font-weight:bold;color:red"><?php echo $total_visa_unactive?></p><p style="margin: 0;padding-left:2px;font-weight:bold;color:red">BELUM LUNAS</p>
                  </div>
              </div>
              <div class="icon">
                <i class="fab fa-cc-visa"></i>
              </div>
              <a href="<?php echo base_url(). 'dashboard/list_visa'; ?>" class="small-box-footer">Lihat Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <?if(isset($total_tiket)){?>
                <h4><?php echo $total_tiket ?></h4>
                <?php  } else {?>
                <h3>0</h3>
                <?}?>
                <p>Tiket</p>
              </div>
              <div class="icon">
              <i class="fas fa-ticket-alt"></i>
              </div>
              <a href="<?php echo base_url(). 'tiket/Tiket/list_tiket'; ?>" class="small-box-footer">Lihat Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

           <!-- ./col -->
           <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-grey">
              <div class="inner">
                <?if(isset($total_tour_travel)){?>
                <h4><?php echo $total_tour_travel ?></h4>
                <?php  } else {?>
                <h3>0</h3>
                <?}?>
                <p>Tour & Travel</p>
              </div>
              <div class="icon">
              <i class="far fa-image"></i>
              </div>
              <a href="<?php echo base_url(). 'tiket/Tiket/list_tiket'; ?>" class="small-box-footer" style="color:#000">Lihat Detail <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>


        </div>
        <hr>
        <div class="row">
            <?php if(isset($oneweek_foto)){?>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 15px;padding-right: 15px;">
              <h5>Jadwal Foto Minggu ini, <?php echo date("d-F-Y")?></h5>
                  <div id="list-visa">
                      <div class="table-responsive">
                          <table class="table table-bordered">
                          <thead>
                          <tr>
                          <th scope="col" class="bg-danger">No</th>
                              <th scope="col" class="bg-danger">Jadwal Foto</th>
                              <th scope="col" class="bg-danger">Nama</th>
                          </tr>
                          </thead>
                          <tbody>
                                <?php 
                                if(isset($oneweek_foto)){
                                $no = 1;
                                foreach($oneweek_foto as $data){ 
                                ?>
                                <tr>
                                    <th scope="row"><?echo $no?></th>
                                      <td><?php
                                      $date = strtotime($data->jadwal_foto);
                                      echo date('d-F-Y', $date)?></td>
                                      <td><?php echo  $data->nama?></td>
                                    </tr>
                                <tr>
                                <? $no++; }} else {?>
                                  <caption><p>Data tidak ada</p></caption>
                                  <?}?>
                            </tbody>
                            </table>
                        </div>
                      </div>
                    </div>
                <?} else {?>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding-left: 15px;padding-right: 15px;">
              <h5>Jadwal Foto Minggu ini, <?php echo date("d-F-Y")?></h5>
                  <div id="list-visa">
                      <div class="table-responsive">
                          <table class="table table-bordered">
                          <thead>
                          <tr>
                          <th scope="col" class="bg-danger">No</th>
                              <th scope="col" class="bg-danger">Jadwal Foto</th>
                              <th scope="col" class="bg-danger">Nama</th>
                          </tr>
                          </thead>
                          <tbody>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
                <?}?>

            <?php if(isset($data_body_keluar)){?>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="padding-left: 15px;padding-right: 15px;">
              <?php if(isset($data_body_title_keluar)) {?>  
                  <h5><?php echo $data_body_title_keluar?></h5>
              <?}?>
                        <div id="list-visa">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                            <thead>
                            <tr>
                            <th scope="col" class="bg-success">No</th>
                                <th scope="col" class="bg-success">Jenis Transaksi</th>
                                <th scope="col" class="bg-success">Total Pengeluaran</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(isset($data_body_keluar)){
                                $no = 1;
                                foreach($data_body_keluar as $data){ 
                                ?>
                              <tr>
                                <th scope="row"><?echo $no?></th>
                                  <td><?php echo $data['nama_transaksi']?></td>
                                  <td><?php echo  number_format($data['keluar_tot'],2,'.','.')?></td>
                                </tr>
                                <tr>
                                  <? $no++; }

                                  } else {?>
                                  <caption><p>Data tidak ada</p></caption>
                                  <?}?>
                            </tbody>
                            </table>
                        </div>
                        </div>
                    </div>
              <?} elseif (isset($data_body_masuk_visa) || isset($data_body_masuk_tiket) ){?>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="padding-left: 15px;padding-right: 15px;">
                    <?php if(isset($data_body_title_masuk) || isset($data_body_masuk_tiket) ) {?>  
                        <h5><?php echo $data_body_title_masuk?></h5>
                    <?}?>
                              <div id="list-visa">
                              <div class="table-responsive">
                                  <table class="table table-bordered">
                                  <thead>
                                  <tr>
                                  <th scope="col" class="bg-info">No</th>
                                      <th scope="col" class="bg-info">Jenis Transaksi</th>
                                      <th scope="col" class="bg-info">Total Pengeluaran</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <?php 
                                  if(isset($data_body_masuk_visa) || isset($data_body_masuk_tiket) ){
                                  $no = 1;
                                  foreach($data_body_masuk_visa as $data){
                                    if($data['id_visa'] !== 0){
                                      $nama = 'Visa';
                                    } 
                                  ?>
                                  <tr>
                                    <th scope="row"><?echo $no?></th>
                                      <td><?php echo $nama?></td>
                                      <td><?php echo  number_format($data['tot_visa'],2,'.','.')?></td>
                                    </tr>
                                  <tr>
                                    <? $no++; }
                                  
                                  foreach($data_body_masuk_tiket as $data){
                                    if($data['id_tiket'] !== 0){
                                      $nama = 'Tiket';
                                    } 
                                  ?>
                                  <tr>
                                    <th scope="row"><?echo $no?></th>
                                      <td><?php echo $nama?></td>
                                      <td><?php echo  number_format($data['tot_tiket'],2,'.','.')?></td>
                                    </tr>
                                  <tr>
                                    <? $no++; }

                                    } else {?>
                                    <caption><p>Data tidak ada</p></caption>
                                    <?}?>
                                  </tbody>
                                  </table>
                              </div>
                              </div>
                          </div>
                <?}?>
           </div>
      </div>
    </section>
    <!-- /.content -->
  </div>

</div>
  <!-- /.content-wrapper -->
  <?
  $footer;
  $script;
  ?>
