
<? $header;
   $sidebar;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $header_title?></h1>
          </div>

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="<?echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

        <div class="row">
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <?php if (!empty($this->session->flashdata('error'))){?>
                            <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('error');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?} else if(!empty($this->session->flashdata('success'))) {?>
                            <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?}?>
                              <div id="input-form-visa">
                                <div class="form-grid-container">
                                  <form action="<?php echo base_url(). 'common/JenisTransaksi/add_jenis_transaksi'; ?>" method="post">
                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="email">Name</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <input type="text" class="form-control" name="nama_transaksi" placeholder="Enter name" id="email">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="email">Deskripsi</label>
                                                <textarea type="text" class="form-control" name="deskripsi" placeholder="Enter deskripsi" id="email"></textarea>
                                              </div>
                                      </div>
                                      <div class="button-submit-container">
                                            <button type="submit" class="btn btn-primary btn-submit-visa">Submit</button>
                                      </div>
                                  </form>
                                  </div>
                              </div>
                            </div>
                          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <div id="list-visa">
                              <div class="table-responsive">
                                  <table class="table">
                                  <thead>
                                    <tr>
                                      <th scope="col" style="border-top: none;">No</th>
                                      <th scope="col" style="border-top: none;">Name</th>
                                      <th scope="col" style="border-top: none;">Keterangan</th>
                                      <th scope="col" style="border-top: none;">Aksi</th>

                                    </tr>
                                  </thead>
                                  <tbody>
                                <?php if(!empty($list_master_jenis_transaksi)){ ?>
                                        <?php 
                                        $no = 1;
                                        foreach($list_master_jenis_transaksi as $data){ 
                                        ?>
                                            <tr>
                                            <th scope="row"><?echo $no?></th>
                                            <td><?php echo $data->nama_transaksi?></td>
                                            <td><?php echo  $data->deskripsi?></td>
                                            <td>
                                            <div class="button-aksi-container">
                                                <button type="button" class="btn btn-info btn-aksi" data-toggle="modal" data-target="#modal-edit<?=$data->id_transaksi;?>"><i class="far fa-edit"></i></button>
                                                <button type="submit" class="btn btn-danger btn-aksi" data-toggle="modal" data-target="#modal_hapus<?php echo $data->id_transaksi;?>"><i class="fas fa-trash-alt"></i></button>
                                                <button type="submit" class="btn btn-warning btn-aksi" data-toggle="modal"><i class="fas fa-file"></i></button>
                                            </div>
                                            </td>
                                            </tr>
                                        <? $no++; }?>
                                <? }?>
                            
                                  </tbody>
                                  </table>
                              </div>
                              </div>
                          </div>
</div>

 <?php $no=0; foreach($list_master_jenis_transaksi as $row): $no++; ?>
<div class="modal fade" id="modal-edit<?=$row->id_transaksi;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Visa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?php echo base_url(). 'common/JenisTransaksi/edit_jenis_transaksi'; ?>" method="post">
                                <div class="row">
                                  <input type="hidden" class="form-control" name="id" value="<?php echo $row->id_transaksi;?>"  />
                                    <div class="form-group col-md-12">
                                          <div style="display:flex">
                                              <label for="email">Name</label>
                                              <div class="text-danger">*</div>
                                          </div>
                                            <input type="text" class="form-control" name="nama_transaksi" value="<?php echo $row->nama_transaksi;?>" placeholder="Enter name" id="email">
                                          </div>
                                          <div class="form-group col-md-12">
                                          <label for="email"> Keterangan</label>
                                              <textarea type="text" class="form-control" name="deskripsi" placeholder="Enter address" id="email"><?php echo $row->deskripsi;?></textarea>
                                          </div>
                                      </div>
                
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-primary" value="Submit" >
                </div>
        </form>
      </div>

    </div>
  </div>
</div>
<?php endforeach; ?>

<?php $no=0; foreach($list_master_jenis_transaksi as $row): $no++; ?>
<div class="modal fade" id="modal_hapus<?php echo $row->id_transaksi;?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'common/JenisTransaksi/delete_jenis_transaksi'; ?>">
            <input type="hidden" class="form-control" name="id" value="<?php echo $row->id_transaksi;?>"  />

                <div class="modal-body">
                    <p>Anda yakin mau menghapus <b><?php echo $row->nama_transaksi;?></b></p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
</div>
<?php endforeach; ?>


<? $footer;
   $script;
?>