<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListVisa extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_dashboard');
	 $this->load->helper('url');
	 $this->load->library('pdf');
    }
	public function index(){
		$data['header_title'] = 'List Visa Terdaftar';
		$data['breadchumb'] = 'List Visa'; 
		$this->load->helper('url');
		$this->load->library('pagination');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
		$data['script'] = $this->load->view('component/script');
		$data['jenis_visa'] = $this->m_dashboard->get_jenis_visa()->result();
        $data['negara'] = $this->m_dashboard->get_negara()->result();
		$data['list_visa_all'] = $this->m_dashboard->get_list_visa()->result();
		
		$data['total_visa'] = count($data['list_visa_all']);
		$data['list_visa_unactive'] = $this->m_dashboard->get_list_unactive()->result();
		$data['total_visa_unactive'] = count($data['list_visa_unactive']);
		$data['list_visa_unactive'] = $this->m_dashboard->get_list_active()->result();
		$data['total_visa_active'] = count($data['list_visa_unactive']);

		$jumlah_income = $this->m_dashboard->get_kas_masuk()->result();
		$data['total_masuk'] = json_decode(json_encode($jumlah_income), true);
		$jumlah_outcome = $this->m_dashboard->get_kas_keluar()->result();
		$data['total_keluar'] = json_decode(json_encode($jumlah_outcome), true);
		$inminout = (int)$data['total_masuk'][0]['jumlah'] - (int)$data['total_keluar'][0]['total'];
		$out = (int)$data['total_keluar'][0]['total'];
		$data['sum_total_masuk'] = "Rp " . number_format($inminout,2,'.','.');
		$data['sum_total_keluar'] = "Rp " . number_format($out,2,'.','.');
		$selisih = abs( $inminout - $out);
		$data['selisih'] = "Rp " . number_format($selisih,2,'.','.');
		
		$this->load->view('common/v_list_visa', $data);
	}
	public function getListVisa(){
		$data['list_visa_all'] = $this->m_dashboard->get_list_visa()->result();
		return $data;
	}

	function edit_visa(){
		$this->db->trans_start();
		$id = $this->input->post('id');
        $nama = $this->input->post('nama');
        $id_jenis_visa = $this->input->post('jenis_visa');
        $no_paspor = $this->input->post('no_paspor');
        $email = $this->input->post('email');
        $expired_v1 = $this->input->post('expired_v1');
        $expired_v2 = $this->input->post('expired_v2');
        $jadwal_foto = $this->input->post('jadwal_foto');
        $keterangan = $this->input->post('keterangan');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $negara = $this->input->post('negara');
        $tgl_transaksi = $this->input->post('tgl_transaksi');
		$total_biaya = str_replace(".","",strtolower(trim($this->input->post('total_biaya'))));
		$dp = str_replace(".","",strtolower(trim($this->input->post('dp'))));
        $sisa = $this->input->post('sisa');
		$status = $this->input->post('status');
		if($total_biaya <= $dp) {
			$status = 1;
		} else {
			$status = 0;
		}
		$data = array(
            'nama' => $nama,
            'id_jenis_visa' => $id_jenis_visa,
            'no_paspor' => $no_paspor,
            'email' => $email,
            'expired_v1' => $expired_v1,
            'expired_v2' => $expired_v2,
            'jadwal_foto' => $jadwal_foto,
            'keterangan' => $keterangan,
            'alamat' => $alamat,
            'telp'  => $telp,
            'id_negara' => $negara,
            'tgl_transaksi' => $tgl_transaksi,
			'total_biaya' => $total_biaya,
			'dp' => $dp,
			'sisa' => str_replace(".","",strtolower(trim($sisa))),
			'status' => $status
		);
		$where = array(
			'id_visa' => $id
		);

		$this->m_dashboard->model_edit_visa($where,$data, 'tb_visa');
		$this->m_dashboard->model_edit_kas_masuk($id, $dp);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Visa Dengan No " . $no_paspor . " Berhasil Diedit" );
			$data_last = $this->m_dashboard->get_list_visa_last()->result();
			$data_last_view= json_decode( json_encode($data_last), true);
			$this->session->set_flashdata('last_added',$data_last_view);
            
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('common/ListVisa');

	}

	function delete_visa(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id_visa' => $id);
		$this->m_dashboard->model_delete_visa($where,'tb_visa');
		$this->m_dashboard->modal_delete_kas_masuk($id);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('common/ListVisa');
    }




function pdf()
{
	$id = $this->input->post('id');
	$dataexp = $this->m_dashboard->get_list_visa_pdf($id)->result();
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->setPrintFooter(false);
	$pdf->setPrintHeader(false);
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
	$pdf->AddPage('');
	$htmlHEADER = '<h1 style="text-align:center">KWITANSI</h1>';
	$pdf->writeHTML($htmlHEADER);
	$pdf->Write(6, '', '', 0, 'R', true, 0, false, false, 0);
	$pdf->Write(6, 'GLOBAL AGENCY', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Write(6, 'Address: JL. Raya Sanggingan 45 Campuhan, Ubud 80571, Gianyar-Bali', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Write(6, 'Phone: +62 361 9080802 / +6281 239 410 158 (WA)', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Write(6, 'EMAIL : global_tsv@yahoo.com', '', 0, 'L', true, 0, false, false, 0);
	$datapdf = json_decode(json_encode($dataexp), true);
	
	foreach ($datapdf as $row ) {  
		$idvisa = '<label style="text-align:right">' . $row['id_visa'] . '</label>';
	}
	$pdf->writeHTML($idvisa);
	$pdf->Write(6, 'Date, .... - .... - 2020 ', '', 0, 'R', true, 0, false, false, 0);
	$pdf->Write(6, '', '', 0, 'R', true, 0, false, false, 0);
	$a = '
	<table border="0.5"  cellpadding="6">
		  <tr>
				<th> <b>Nama</b> </th>
				<th> <b>No Paspor</b></th>
				<th> <b>Jenis Visa</b></th>
				<th> <b>Jadwal Foto</b></th>
				<th> <b>Total</b></th>

		  </tr>
	';

	foreach ($datapdf as $row ) {  
		$b = '<tr>
		<td><p style="font-size:10px"> ' . $row['nama'] . '</p></td>
		<td><p style="font-size:10px"> '.$row['no_paspor'].'</p></td>
		<td><p style="font-size:10px"> '.$row['nama_visa'].'</p></td>
		<td><p style="font-size:10px"> '.$row['jadwal_foto'].'</p></td>
		<td><p style="font-size:10px"> '.number_format($row['total_biaya'],2,'.','.').'</p></td>
		</tr>';
	}
		$c = '</table><br/><br/><br/><br/>';

	foreach ($datapdf as $data ) { 
		$e = '
		<table border="0">
			<tr>
					<th>Mengetahui</th>
					<th></th>
					<th>Pihak Global Visagency</th>
			</tr>
			<br/><br/><br/><br/>
			<tr>
					<th>'. $data['nama'] .'</th>
					<th></th>
					<th>Global Visagency</th>
			</tr>
		</table>
		';
	}
	$pdf->writeHTML($a.$b.$c.$e);
	$pdf->Output('file-pdf-codeigniter.pdf', 'I');
}

}