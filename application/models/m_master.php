<?php 
 
class M_master extends CI_Model{

    function get_list_master_jenis_visa(){
        $data = $this->db->query('SELECT * FROM `tb_jenis_visa` WHERE 1');
        return $data;
    }

    function model_add_jenis_visa($data, $table){
        $this->db->insert($table,$data);
    }
    function model_edit_jenis_visa($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    function model_delete_jenis_visa($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
  }
  


  function model_add_jenis_transaksi($data, $table){
    $this->db->insert($table,$data);
    }
    function get_list_master_jenis_transaksi(){
        $data = $this->db->query('SELECT * FROM `tb_jenis_transaksi` WHERE 1');
        return $data;
    }
    function model_edit_jenis_transaksi($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
    }
    function model_delete_jenis_transaksi($where,$table){
        $this->db->where($where);
		$this->db->delete($table);
    }



        function model_add_jenis_maskapai($data, $table){
        $this->db->insert($table,$data);
        }
        function get_list_master_jenis_maskapai(){
            $data = $this->db->query('SELECT * FROM `tb_maskapai` WHERE 1');
            return $data;
        }
        function model_edit_jenis_maskapai($where,$data,$table){
                $this->db->where($where);
                $this->db->update($table,$data);
        }
        function model_delete_jenis_maskapai($where,$table){
            $this->db->where($where);
            $this->db->delete($table);
        }



        function model_add_jenis_tour($data, $table){
            $this->db->insert($table,$data);
        }
        function get_list_master_jenis_tour(){
            $data = $this->db->query('SELECT * FROM `tb_jenis_tour` WHERE 1');
            return $data;
        }
        function model_edit_jenis_tour($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
        }
        function model_delete_jenis_tour($where,$table){
            $this->db->where($where);
            $this->db->delete($table);
        }
    
}