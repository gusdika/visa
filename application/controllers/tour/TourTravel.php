<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TourTravel extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
     $this->load->model('m_master');
     $this->load->model('m_dashboard');
	 $this->load->model('m_tourtravel');
     
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Input Tour & Travel';
		$data['breadchumb'] = 'Tour & Travel'; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['list_tour_travel_last'] = $this->m_dashboard->get_list_visa_last()->result();
        $data['jenis_tour'] = $this->m_master->get_list_master_jenis_tour()->result();
        $data['negara'] = $this->m_dashboard->get_negara()->result();
        
        $this->load->view('tour/v_tour_travel', $data);
    }

    public function add_tour_travel(){
		if(!empty($this->input->post('nama') && $this->input->post('dari') && $this->input->post('no_paspor') && $this->input->post('sampai') && $this->input->post('id_tour') )){
			$nama = $this->input->post('nama');
			$tgl_dari = $this->input->post('dari');
			$tgl_sampai = $this->input->post('sampai');
            $no_paspor = $this->input->post('no_paspor');
            $negara = $this->input->post('negara');
			$id_tour = $this->input->post('id_tour');
			$keterangan = $this->input->post('keterangan');
			$alamat = $this->input->post('alamat');
            $total_biaya = str_replace(".","",strtolower(trim($this->input->post('total_biaya'))));
            $dp = str_replace(".","",strtolower(trim($this->input->post('dp'))));
            $sisa = $this->input->post('sisa');
			if($total_biaya <= $dp) {
				$status = 1;
			} else {
				$status = 0;
			}
			$data = array(
				'nama' => $nama,
				'tgl_dari' => $tgl_dari,
                'tgl_sampai' => $tgl_sampai,
				'no_paspor' => $no_paspor,
				'id_tour' => $id_tour,
				'keterangan' => $keterangan,
                'alamat' => $alamat,
                'id_negara' => $negara,
				'total_biaya' => $total_biaya,
				'dp' => $dp,
				'sisa' => str_replace(".","",strtolower(trim($sisa))),
				'status' => $status
				);
			$res = $this->m_tourtravel->model_add_tour_travel($data,'tb_tour_travel');
			$this->session->set_flashdata('success', "Visa Berhasil Didaftarkan"); 
			$data_last = $this->m_tourtravel->get_list_tour_travel_last()->result();
			$data_last_view = json_decode( json_encode($data_last), true);
			$array = json_decode( json_encode($data_last), true);
			foreach ($array as $result) {
				$income = array(
					'jum' => $result['dp'],
					'id_tt' => $result['id_tt']
				);
				$masuk_kas = $this->m_dashboard->add_kas_masuk($income,'tb_income');
			}


			$this->session->set_flashdata('last_added',$data_last_view);
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('tour/TourTravel');
	}

}