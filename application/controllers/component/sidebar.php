<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends CI_Controller {

	public function index()
	{
		// ----------------------------
		// format penulisan: array(
		//   'Teks|ikon|alamat_link', 
		// );
		// ----------------------------
		
		$this->load->view('component/sidebar.php');
		
	}
	public function input_visa(){
		redirect('common/InputVisa');
	}
}
