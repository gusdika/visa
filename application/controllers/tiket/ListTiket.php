<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListTiket extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
     $this->load->model('m_dashboard');
	 $this->load->model('m_tiket');
	 $this->load->helper('url');
	 $this->load->library('pdf');

    }
	public function index(){
		$data['header_title'] = 'List Tiket Terdaftar';
		$data['breadchumb'] = 'List Tiket'; 
		$this->load->helper('url');
		$this->load->library('pagination');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
		$data['script'] = $this->load->view('component/script');
		$data['negara'] = $this->m_dashboard->get_negara()->result();
        $data['list_tiket_last'] = $this->m_tiket->get_list_tiket_last()->result();
        $data['jenis_maskapai'] = $this->m_tiket->get_master_jenis_maskapai()->result();
		
		$data['list_tiket_all'] =  $this->m_tiket->get_list_tiket()->result();
		$data['total_tiket'] = count($data['list_tiket_all']);
		$this->load->view('tiket/v_list_tiket', $data);
	}
	public function getListTiket(){
		$data['list_tiket_all'] = $this->m_tiket->get_list_tiket()->result();
		return $data;
	}

	public function search_tiket(){
		if(!empty($this->input->post('word'))){
			$word = $this->input->post('word');
		}else{
			$word = '';
		}
		$hasil = $this->m_tiket->model_search_tiket($word)->result();
		$this->session->set_flashdata('pass',$word);
		$this->session->set_flashdata('on',$hasil);
		redirect('tiket/SearchTiket');
	}
	
	function search_by(){
		if($this->input->post('sort_by') == 'Search by'){
			$hasil = $this->m_tiket->get_list_tiket()->result();
		}else{
			$word = $this->input->post('sort_by');
			$hasil = $this->m_tiket->model_search_by($word)->result();
			if($word == '1'){
				$word = 'Lunas';
			}else if($word == '0'){
				$word = 'Belum Lunas';
			}
			$this->session->set_flashdata('pass',$word);
		}
		$this->session->set_flashdata('on',$hasil);
		redirect('tiket/SearchTiket');
	}

	function search_date(){
		if(empty($this->input->post('date_filter'))){
			$hasil = $this->m_tiket->get_list_tiket()->result();
		}else{
			$word = strval($this->input->post('date_filter'));
			$hasil = $this->m_tiket->model_search_date($word)->result();
			$this->session->set_flashdata('pass',$word);
		}
		$this->session->set_flashdata('on',$hasil);
		redirect('tiket/SearchTiket');
	}


	function search_mounth_year(){
		if(empty($this->input->post('month') || $this->input->post('year'))){
			$hasil = '';
			$month = $this->input->post('month');
			$year = $this->input->post('year');
		}else{
			$month = $this->input->post('month');
			$year = $this->input->post('year');
			$hasil = $this->m_tiket->model_search_month_year($month, $year)->result();
			
			if($month == '01'){
				$month = 'Januari';
			}else if($month == '02'){
				$month = 'Februari';
			}else if($month == '03'){
				$month = 'Maret';
			}else if($month == '04'){
				$month = 'April';
			}else if($month == '05'){
				$month = 'Mei';
			}else if($month == '06'){
				$month = 'Juni';
			}else if($month == '07'){
				$month = 'Juli';
			}else if($month == '08'){
				$month = 'Agustus';
			}else if($month == '09'){
				$month = 'September';
			}else if($month == '10'){
				$month = 'Oktober';
			}else if($month == '11'){
				$month = 'November';
			}else if($month == '12'){
				$month = 'Desember';
			}
			$wordSearch = sprintf('%s %s',$month,$year);
			$this->session->set_flashdata('pass',$wordSearch);

		}
		$this->session->set_flashdata('on',$hasil);
		redirect('tiket/SearchTiket');
	}

	function edit_tiket(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$nama = $this->input->post('nama');
		$id_maskapai = $this->input->post('id_maskapai');
		$no_paspor = $this->input->post('no_paspor');
		$t_lahir = $this->input->post('t_lahir');
		$telp = $this->input->post('telp');
		$t_expired = $this->input->post('t_expired');
		$rute = $this->input->post('rute');
		$id_negara = $this->input->post('id_negara');
		$no_penerbangan = $this->input->post('no_penerbangan');
		$time_destinasi = $this->input->post('time_destination');
		$tiket_dewasa = $this->input->post('tiket_dewasa');
		$tiket_anak = $this->input->post('tiket_anak');
		$per_tiket = $this->input->post('per_tiket');
		$total = str_replace(".","",strtolower(trim($this->input->post('total'))));
		$dp = str_replace(".","",strtolower(trim($this->input->post('dp'))));
		$sisa = $this->input->post('sisa');
		if($total <= $dp) {
			$status = 1;
		} else {
			$status = 0;
		}
		$data = array(
			'nama' => $nama,
			'id_maskapai' => $id_maskapai,
			'no_paspor' => $no_paspor,
			't_lahir' => $t_lahir,
			'telp' => $telp,
			't_expired' => $t_expired,
			'rute' => $rute,
			'no_penerbangan' => $no_penerbangan,
			'id_negara'     => $id_negara,
			'time_destinasi' => $time_destinasi,
			'tiket_dewasa' => $tiket_dewasa,
			'tiket_anak'  => $tiket_anak,
			'per_tiket' => str_replace(".","",strtolower(trim($per_tiket))),
			'total' => $total,
			'dp' => $dp,
			'sisa' => str_replace(".","",strtolower(trim($sisa))),
			'status' => $status
			);

		$where = array(
			'id_tiket' => $id
		);

		$this->m_tiket->model_edit_tiket($where,$data,'tb_tiket');
		$this->m_dashboard->model_edit_kas_masuk_tiket($id, $dp);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Visa Dengan No " . $no_paspor . " Berhasil Diedit" );
			$data_last = $this->m_tiket->get_list_tiket_last()->result();
			$data_last_view= json_decode(json_encode($data_last), true);
			$this->session->set_flashdata('last_added',$data_last_view);
            
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('tiket/ListTiket');

	}

	function delete_tiket(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id_tiket' => $id);
		$this->m_tiket->model_delete_tiket($where,'tb_tiket');
		$this->m_dashboard->modal_delete_kas_masuk_tiket($id);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('tiket/ListTiket');
	}

	function pdf()
{
	$id = $this->input->post('id');
	$dataexp = $this->m_tiket->get_list_tiket_pdf($id)->result();
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
	$pdf->setPrintFooter(false);
	$pdf->setPrintHeader(false);
	$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);
	$pdf->AddPage('');
	$htmlHEADER = '<h1 style="text-align:center">KWITANSI</h1>';
	$pdf->writeHTML($htmlHEADER);
	$pdf->Write(6, '', '', 0, 'R', true, 0, false, false, 0);
	$pdf->Write(6, 'GLOBAL AGENCY', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Write(6, 'Address: JL. Raya Sanggingan 45 Campuhan, Ubud 80571, Gianyar-Bali', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Write(6, 'Phone: +62 361 9080802 / +6281 239 410 158 (WA)', '', 0, 'L', true, 0, false, false, 0);
	$pdf->Write(6, 'EMAIL : global_tsv@yahoo.com', '', 0, 'L', true, 0, false, false, 0);
	$datapdf = json_decode(json_encode($dataexp), true);
	
	foreach ($datapdf as $row ) {  
		$idtiket = '<label style="text-align:right">' . $row['id_tiket'] . '</label>';
	}
	$pdf->writeHTML($idtiket);
	$pdf->Write(6, 'Date, .... - .... - 2020 ', '', 0, 'R', true, 0, false, false, 0);
	$pdf->Write(6, '', '', 0, 'R', true, 0, false, false, 0);

	// $htmlSubHEADER = '<p>GLOBAL AGENCY</p>
	// <p>Address: JL. Raya Sanggingan 45 Campuhan, Ubud 80571, Gianyar-Bali</p>
	// <p>Phone: +62 361 9080802 / +6281 239 410 158 (WA)</p>
	// <p>EMAIL : global_tsv@yahoo.com</p>
	// <p style="text-align:right">Date, .... - .... - 2020</p>
	// ';
	$a = '
	<table border="0.5"  cellpadding="6">
		  <tr>
				<th> <b>Nama</b> </th>
				<th> <b>No Paspor</b></th>
				<th> <b>Jenis Maskapai</b></th>
				<th> <b>No Penerbangan</b></th>
				<th> <b>Tiket</b></th>
				<th> <b>Total</b></th>

		  </tr>
	';

	foreach ($datapdf as $row ) {  
		$b = '<tr>
		
		<td><p style="font-size:10px">' . $row['nama'] . '</p></td>
		<td><p style="font-size:10px">'.$row['no_paspor'].'</p></td>
		<td><p style="font-size:10px">'.$row['nama_maskapai'].'</p></td>
		<td><p style="font-size:10px">'.$row['no_penerbangan'].'</p></td>
		<td><p style="font-size:10px">'. $row['tiket_dewasa']. ' Dewasa' . '</p><p style="font-size:10px">'. $row['tiket_anak']. ' Anak' . '</p></td>
		<td> '.number_format($row['total'],2,'.','.').'</td>
		
		</tr>';
	}
		$c = '</table><br/><br/><br/><br/>';

	foreach ($datapdf as $data ) { 
		$e = '
		<table border="0">
			<tr>
					<th>Mengetahui</th>
					<th></th>
					<th>Pihak Global Visagency</th>
			</tr>
			<br/><br/><br/><br/>
			<tr>
					<th>'. $data['nama'] .'</th>
					<th></th>
					<th>Global Visagency</th>
			</tr>
		</table>
		';
	}
	$pdf->writeHTML($a.$b.$c.$e);
	$pdf->Output('file-pdf-codeigniter.pdf', 'I');
	}

}
