<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('m_dashboard');
		$this->load->model('m_tiket');
		$this->load->model('m_tourtravel');
		
		$this->load->helper('url');
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}
	public function index()
	{	
		$this->load->helper('url');
		$data['breadchumb'] = 'Home'; 
		$data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
		$data['script'] = $this->load->view('component/script');
		$data['list_visa'] = $this->m_dashboard->get_visa()->result();
		
		$data['data_body_masuk_visa'] = $this->session->flashdata('pass-masuk-visa');
		$data['data_body_masuk_tiket'] = $this->session->flashdata('pass-masuk-tiket');
		$data['data_body_title_masuk'] = $this->session->flashdata('title-body-masuk');
		

		$data['data_body_keluar'] = $this->session->flashdata('pass');
		$data['data_body_title_keluar'] = $this->session->flashdata('title-body');

		$data['list_tiket_all'] =  $this->m_tiket->get_list_tiket()->result();
		$data['total_tiket'] = count($data['list_tiket_all']);

		$data['list_tour_travel_all'] =  $this->m_tourtravel->get_list_tour_travel()->result();
		$data['total_tour_travel'] = count($data['list_tour_travel_all']);

		$data['list_visa_all'] = $this->m_dashboard->get_list_visa()->result();
		$data['total_visa'] = count($data['list_visa_all']);
		$data['list_visa_unactive'] = $this->m_dashboard->get_list_unactive()->result();
		$data['total_visa_unactive'] = count($data['list_visa_unactive']);
		$data['list_visa_unactive'] = $this->m_dashboard->get_list_active()->result();
		$data['total_visa_active'] = count($data['list_visa_unactive']);

		$jumlah_income = $this->m_dashboard->get_kas_masuk()->result();
		$data['total_masuk'] = json_decode(json_encode($jumlah_income), true);
		$jumlah_outcome = $this->m_dashboard->get_kas_keluar()->result();
		$data['total_keluar'] = json_decode(json_encode($jumlah_outcome), true);
		$inminout = (int)$data['total_masuk'][0]['jumlah'] - (int)$data['total_keluar'][0]['total'];
		$out = (int)$data['total_keluar'][0]['total'];
		$data['sum_total_masuk'] = "Rp " . number_format($inminout,2,'.','.');
		$data['sum_total_keluar'] = "Rp " . number_format($out,2,'.','.');
		$selisih = abs( $inminout - $out);
		$data['selisih'] = "Rp " . number_format($selisih,2,'.','.');
		$data['oneweek_foto'] = $this->m_dashboard->get_week_jadwal_foto()->result();
		$this->load->view('dashboard', $data);
		
	}

	

	function delete_visa(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id_visa' => $id);
		$this->m_dashboard->model_delete_visa($where,'tb_visa');
		$this->m_dashboard->modal_delete_kas_masuk($id);
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('common/ListVisa');
	}

	public function list_visa(){
		redirect('common/ListVisa');
	}
	public function kas_keluar(){
		redirect('kas/KasKeluar');
	}
	public function search_visa(){
		if(!empty($this->input->post('word'))){
			$word = $this->input->post('word');
		}else{
			$word = '';
		}
		$hasil = $this->m_dashboard->model_search_visa($word)->result();
		$this->session->set_flashdata('pass',$word);
		$this->session->set_flashdata('on',$hasil);
		redirect('common/SearchVisa');
	}
	
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}

	function search_by(){
		if($this->input->post('sort_by') == 'Search by'){
			$hasil = $this->m_dashboard->get_list_visa()->result();
		}else{
			$word = $this->input->post('sort_by');
			$hasil = $this->m_dashboard->model_search_by($word)->result();
			if($word == '1'){
				$word = 'Lunas';
			}else if($word == '0'){
				$word = 'Belum Lunas';
			}
			$this->session->set_flashdata('pass',$word);
		}
		$this->session->set_flashdata('on',$hasil);
		redirect('common/SearchVisa');
	}

	function search_date(){
		if(empty($this->input->post('date_filter'))){
			$hasil = $this->m_dashboard->get_list_visa()->result();
		}else{
			$word = strval($this->input->post('date_filter'));
			$hasil = $this->m_dashboard->model_search_date($word)->result();
			$this->session->set_flashdata('pass',$word);
		}
		$this->session->set_flashdata('on',$hasil);
		redirect('common/SearchVisa');
	}


	function search_mounth_year(){
		if(empty($this->input->post('month') || $this->input->post('year'))){
			$hasil = '';
			$month = $this->input->post('month');
			$year = $this->input->post('year');
		}else{
			$month = $this->input->post('month');
			$year = $this->input->post('year');
			$hasil = $this->m_dashboard->model_search_month_year($month, $year)->result();
			
			if($month == '01'){
				$month = 'Januari';
			}else if($month == '02'){
				$month = 'Februari';
			}else if($month == '03'){
				$month = 'Maret';
			}else if($month == '04'){
				$month = 'April';
			}else if($month == '05'){
				$month = 'Mei';
			}else if($month == '06'){
				$month = 'Juni';
			}else if($month == '07'){
				$month = 'Juli';
			}else if($month == '08'){
				$month = 'Agustus';
			}else if($month == '09'){
				$month = 'September';
			}else if($month == '10'){
				$month = 'Oktober';
			}else if($month == '11'){
				$month = 'November';
			}else if($month == '12'){
				$month = 'Desember';
			}
			$wordSearch = sprintf('%s %s',$month,$year);
			$this->session->set_flashdata('pass',$wordSearch);

		}
		$this->session->set_flashdata('on',$hasil);
		redirect('common/SearchVisa');
	}

	function GetListKasKeluar(){
		$hasil = $this->m_dashboard->get_list_kas_keluar_jum()->result();
		$keluar = json_decode(json_encode($hasil), true);
		$this->session->set_flashdata('pass',$keluar);
		$this->session->set_flashdata('title-body','List Kas Keluar');
		redirect('dashboard');
	}

	function GetListKasMasuk(){
		$hasilvisa = $this->m_dashboard->get_list_kas_masuk_jum_visa()->result();
		$masukvisa = json_decode(json_encode($hasilvisa), true);

		$hasiltiket = $this->m_dashboard->get_list_kas_masuk_jum_tiket()->result();
		$masuktiket = json_decode(json_encode($hasiltiket), true);

		$this->session->set_flashdata('pass-masuk-visa',$masukvisa);
		$this->session->set_flashdata('pass-masuk-tiket',$masuktiket);
		$this->session->set_flashdata('title-body-masuk','List Kas Masuk');
		redirect('dashboard');
	}
	
}
