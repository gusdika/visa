<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JenisTransaksi extends CI_Controller {
	public function __construct()
    {
	 parent::__construct();
	 $this->load->model('m_master');
	 $this->load->helper('url');
    }
    public function index(){
        $data['header_title'] = 'Master Jenis Trasaksi';
		$data['breadchumb'] = 'Master Transaksi '; 
		$this->load->helper('url');
        $data['header'] = $this->load->view('component/header');
		$data['sidebar'] = $this->load->view('component/sidebar');
		$data['footer'] = $this->load->view('component/footer');
        $data['script'] = $this->load->view('component/script');
        $data['list_master_jenis_transaksi'] = $this->m_master->get_list_master_jenis_transaksi()->result();

        $this->load->view('common/v_master_jenis_transaksi', $data);
    }

    public function add_jenis_transaksi(){
		if(!empty($this->input->post('nama_transaksi') || $this->input->post('deskripsi'))){
			$nama_transaksi = $this->input->post('nama_transaksi');
            $deskripsi = $this->input->post('deskripsi');
			$data = array(
				'nama_transaksi' => $nama_transaksi,
				'deskripsi' => $deskripsi,
				);

			$res = $this->m_master->model_add_jenis_transaksi($data,'tb_jenis_transaksi');
			$this->session->set_flashdata('success', "Transaksi Berhasil Didaftarkan"); 
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}
		redirect('common/JenisTransaksi');
    }

    public function edit_jenis_transaksi(){
        $this->db->trans_start();
		$this->load->helper('url');
		$id = $this->input->post('id');
        $nama_transaksi = $this->input->post('nama_transaksi');
        $deskripsi = $this->input->post('deskripsi');

        $data = array(
            'nama_transaksi' => $nama_transaksi,
            'deskripsi' => $deskripsi,
            );
        
        $where = array(
            'id_transaksi' => $id
        );

		$this->m_master->model_edit_jenis_transaksi($where,$data,'tb_jenis_transaksi');
		$this->db->trans_complete();
        if($this->db->trans_status() === TRUE){
            $this->session->set_flashdata('success', "Transaksi " . $nama_transaksi . " Berhasil Diedit" );
		}else{
			$this->session->set_flashdata('error', "Mohon Isi From Dengan Lengkap");
		}

		redirect('common/JenisTransaksi');
    }

    function delete_jenis_transaksi(){
		$this->db->trans_start();
		$id = $this->input->post('id');
		$where = array('id_transaksi' => $id);
		$this->m_master->model_delete_jenis_transaksi($where,'tb_jenis_transaksi');
		$this->db->trans_complete();
		if($this->db->trans_status() === TRUE){
			$this->session->set_flashdata('success', "Data Berhasil Dihapus" ); 
		}
		redirect('common/JenisTransaksi');
    }
}