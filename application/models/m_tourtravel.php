<?php 
 
class M_tourtravel extends CI_Model{

    function model_add_tour_travel($data, $table){
            $this->db->insert($table,$data);
    }

    function get_list_tour_travel_last(){
        $data = $this->db->query('SELECT * FROM `tb_tour_travel` tt LEFT JOIN tb_jenis_tour tj ON tt.id_tour = tj.id_jenis_tour LEFT JOIN apps_countries ap ON tt.id_negara = ap.id ORDER BY tt.id_tt DESC LIMIT 1');
        return $data;
    }
    function get_list_tour_travel() {
        $data = $this->db->query('SELECT * FROM `tb_tour_travel` tt LEFT JOIN tb_jenis_tour tj ON tt.id_tour = tj.id_jenis_tour LEFT JOIN apps_countries ap ON tt.id_negara = ap.id ORDER BY tt.id_tt ');
        return $data;
    }
}