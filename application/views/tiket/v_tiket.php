
<? $header;
   $sidebar;
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0 text-dark"><?php echo $header_title?></h5>
          </div>

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item "><a href="<?echo base_url('dashboard'); ?>">Home</a></li>
              <li class="breadcrumb-item active"><?php echo $breadchumb?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

        <div class="row">
                          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                          <?php if (!empty($this->session->flashdata('error'))){?>
                            <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i> <?php echo $this->session->flashdata('error');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?} else if(!empty($this->session->flashdata('success'))) {?>
                            <div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> <?php echo $this->session->flashdata('success');?>
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                            </div>
                          <?}?>
                              <div id="input-form-visa">
                                <div class="form-grid-container">
                                <form action="<?php echo base_url(). 'tiket/Tiket/add_tiket'; ?>" method="post">
                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="email">Nama</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <input type="text" class="form-control" name="nama" placeholder="Masukan nama">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <div style="display:flex">
                                                  <label for="pwd">Jenis Maskapai</label>
                                                  <div class="text-danger">*</div>
                                                </div>
                                                <div class="dropdown">
                                                        <select class="form-control" name="id_maskapai" required autocomplete="off">
                                                                <option selected>Pilih Maskapai</option>
                                                            <?php foreach($jenis_maskapai as $result){?>
                                                                <option value="<?php echo $result->id?>"><?php echo $result->nama_maskapai?></option>
                                                            <? } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                              </div>
                                            
                                      </div>
                                      <div class="row">
                                          <div class="form-group col-md-6">
                                            <div style="display:flex">
                                                <label for="email">No Paspor</label>
                                                <div class="text-danger">*</div>
                                            </div>
                                            <input type="text" class="form-control" name="no_paspor" placeholder="Enter no visa" id="email">
                                          </div>
                                          <div class="form-group col-md-6">
                                              <div style="display:flex">
                                                <label for="pwd">Negara</label>
                                              </div>
                                              <div class="dropdown">
                                                          <select class="form-control" name="id_negara" required autocomplete="off">
                                                              <option selected>Pilih Negara</option>
                                                          <?php foreach($negara as $result){?>
                                                              <option value="<?php echo $result->id?>"><?php echo $result->country_name?></option>
                                                          <? } ?>
                                                          </select>
                                              </div>
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Tanggal Lahir</label>
                                            <input type="date" name="t_lahir" class="form-control">
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Expired</label>
                                                  <input type="date" name="t_expired" class="form-control">
                                            </div>
                                      </div>
                                      <div class="row">
                                              <div class="form-group col-md-6">
                                                <label for="email">Rute</label>
                                                <input type="text" class="form-control" name="rute" placeholder="rute">
                                              </div>
                                              <div class="form-group col-md-6">
                                                <label for="pwd">No Penerbangan</label>
                                                <input type="number" class="form-control" name="no_penerbangan" placeholder="no penerbangan">
                                              </div>
                                      </div>
                                      <div class="row hitung">
                                           <div class="form-group col-md-6">
                                                <label>No Telp</label>
                                                <input type="number" class="form-control" name="telp" placeholder="No Telp">
                                            </div>
                                          <div class="form-group col-md-6">
                                          <label for="birthday">Time Destination</label>
                                                <input type="text" class="form-control" name="time_destination" placeholder="Waktu yang ditempuh">
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="birthday">Dewasa</label>
                                                  <input type="text" class="form-control" name="tiket_dewasa" placeholder="Jumlah Tiket Dewasa" id="jum_dewasa">
                                          </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">Anak</label>
                                                <input type="text" class="form-control" name="tiket_anak" placeholder="Jumlah Tiket Anak" id="jum_anak">
                                          </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">Biaya Pertiket</label>
                                                <input type="text" class="per_tiket form-control" name="per_tiket" placeholder="Total" id="per_tiket">
                                          </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">DP</label>
                                                <input type="text" class="dp form-control" name="dp" placeholder="dp" id="dp">
                                          </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">Total Biaya</label>
                                                <input type="text" class="total form-control" name="total" placeholder="Total" id="total" readonly="readonly">
                                          </div>
                                          <div class="form-group col-md-6">
                                                <label for="birthday">Sisa</label>
                                                <input type="text" class="sisa form-control" name="sisa" placeholder="Sisa" id="sisa" readonly="readonly">
                                          </div>
                                      </div>
                                      <div class="button-submit-container">
                                            <button type="submit" class="btn btn-primary btn-submit-visa">Submit</button>
                                      </div>
                                  </form>
                                  </div>
                              </div>
                              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                <div id="list-visa">
                                <div class="table-responsive">
                                    <table class="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">No</th>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Maskapai</th>
                                        <th scope="col">Negara</th>
                                        <th scope="col">No Paspor</th>
                                        <th scope="col">TTL</th>
                                        <th scope="col">No Telp</th>
                                        <th scope="col">Expired</th>
                                        <th scope="col">Rute</th>
                                        <th scope="col">No Penerbangan</th>
                                        <th scope="col">Waktu Destinasi</th>
                                        <th scope="col">Tiket Dewasa</th>
                                        <th scope="col">Tiket nak</th>
                                        <th scope="col">Harga Pertiket</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">DP</th>
                                        <th scope="col">Sisa</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Aksi</th>

                                        </tr>
                                    </thead>
                                  <caption><a href="<?php echo base_url(). 'tiket/Tiket/list_tiket'; ?>">Lihat Semua Data Tiket</a></caption>
                                    <tbody>
                                    <?php if(!empty($this->session->flashdata('last_added'))){ ?>
                                        <?php 
                                        $no = 1;
                                        foreach($this->session->flashdata('last_added') as $data){ 
                                        ?>
                                                <tr>
                                                <th scope="row"><?echo $no?></th>
                                                <td><?php echo $data['nama']?></td>
                                                <td><?php echo  $data['nama_maskapai']?></td>
                                                <td><?php echo  $data['country_name']?></td>
                                                <td><?php echo  $data['no_paspor']?></td>
                                                <td><?php echo  $data['t_lahir']?></td>
                                                <td><?php echo  $data['telp']?></td>
                                                <td><?php echo  $data['t_expired']?></td>
                                                <td><?php echo  $data['rute']?></td>
                                                <td><?php echo  $data['no_penerbangan']?></td>
                                                <td><?php echo  $data['time_destinasi']?></td>
                                                <td><?php echo  $data['tiket_dewasa']?></td>
                                                <td><?php echo  $data['tiket_anak']?></td>
                                                <td><?php echo  $data['per_tiket']?></td>
                                                <td><?php echo  $data['total']?></td>
                                                <td><?php echo  $data['dp']?></td>
                                                <td><?php echo  $data['sisa']?></td>
                                                <td><?php echo  $data['status']?></td>


                                                <td>
                                                <div class="button-aksi-container">
                                                    <button type="button" class="btn btn-info btn-aksi" data-toggle="modal" data-target="#modal-edit<?=$data['id_tiket'];?>"><i class="far fa-edit"></i></button>
                                                    <button type="submit" class="btn btn-danger btn-aksi" data-toggle="modal" data-target="#modal_hapus<?php echo $data['id_tiket'];?>"><i class="fas fa-trash-alt"></i></button>
                                                    <!-- <button type="submit" class="btn btn-warning btn-aksi" data-toggle="modal"><i class="fas fa-file"></i></button> -->
                                                </div>
                                                </td>
                                                </tr>
                                            <? $no++; }?>
                                    <? }?>
                                
                                    </tbody>
                                    </table>
                                </div>
                                </div>
                          </div>
                </div>
                          
</div>

<?php if(!empty($this->session->flashdata('last_added'))){ ?>
<?php $no=0; foreach($this->session->flashdata('last_added') as $row): $no++; ?>
<div class="modal fade" id="modal-edit<?=$row['id_tiket'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Visa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form action="<?php echo base_url(). 'tiket/Tiket/edit_tiket'; ?>" method="post">
                          <div class="row">
                            <input type="hidden" class="form-control" name="id" value="<?php echo $row['id_tiket']?>"  />
                                  <div class="form-group col-md-6">
                                      <div style="display:flex">
                                        <label for="email">Nama</label>
                                        <div class="text-danger">*</div>
                                      </div>
                                      <input type="text" class="form-control" value="<?php echo $row['nama'] ?>" name="nama" placeholder="Masukan nama">
                                  </div>
                                  <div class="form-group col-md-6">
                                      <div style="display:flex">
                                        <label for="pwd">Jenis Maskapai</label>
                                        <div class="text-danger">*</div>
                                      </div>
                                        <div class="dropdown">
                                            <select class="form-control" name="id_maskapai" required autocomplete="off">
                                                    <option value="<?php echo $row['id_maskapai'] ?>" selected><?php echo $row['nama_maskapai'] ?></option>
                                                <?php foreach($jenis_maskapai as $result){?>
                                                    <option value="<?php echo $result->id?>"><?php echo $result->nama_maskapai?></option>
                                                <? } ?>
                                            </select>
                                        </div>
                                  </div>
                                
                          </div>
                          <div class="row">
                                  <div class="form-group col-md-6">
                                    <div style="display:flex">
                                        <label for="email">No Paspor</label>
                                        <div class="text-danger">*</div>
                                    </div>
                                    <input type="text" class="form-control" value="<?php echo $row['no_paspor'] ?>"  name="no_paspor" placeholder="Enter no visa" id="email">
                                  </div>
                                  <div class="form-group col-md-6">
                                  <div style="display:flex">
                                      <label for="pwd">Negara</label>
                                    </div>
                                    <div class="dropdown">
                                                <select class="form-control" name="id_negara" required autocomplete="off">
                                                    <option value="<?php echo $row['id_negara']?>" selected><?php echo $row['country_name']?></option>
                                                <?php foreach($negara as $result){?>
                                                    <option value="<?php echo $result->id?>"><?php echo  $result->country_name?></option>
                                                <? } ?>
                                                </select>
                                    </div>
                                </div>
                            <div class="form-group col-md-6">
                                <label for="birthday">Tanggal Lahir</label>
                                <input type="date" name="t_lahir" value="<?php echo $row['t_lahir']?>" class="form-control">
                              </div>
                              <div class="form-group col-md-6">
                              <label for="birthday">Expired</label>
                                    <input type="date" name="t_expired" value="<?php echo $row['t_expired']?>" class="form-control">
                              </div>
                          </div>

                          <div class="row">
                                  <div class="form-group col-md-6">
                                    <label for="email">Rute</label>
                                    <input type="text" class="form-control" name="rute" value="<?php echo $row['rute']?>" placeholder="Masukan rute">
                                  </div>
                                  <div class="form-group col-md-6">
                                    <label for="pwd">No Penerbangan</label>
                                    <input type="number" class="form-control" name="no_penerbangan" value="<?php echo $row['no_penerbangan']?>" placeholder="No penerbangan">
                                  </div>
                                
                          </div>
                          <div class="row hitung-modal">
                              <div class="form-group col-md-6">
                                  <label>No Telp</label>
                                  <input type="text" class="form-control" value="<?php echo $row['telp']?>" name="telp" placeholder="No Telp">
                              </div>
                              <div class="form-group col-md-6">
                                  <label for="birthday">Time Destination</label>
                                    <input type="text" class="form-control" name="time_destination" value="<?php echo $row['time_destinasi']?>" placeholder="Waktu yang ditempuh">
                              </div>
                              <div class="form-group col-md-6">
                                <label for="birthday">Dewasa</label>
                                      <input type="text" class="form-control" name="tiket_dewasa" value="<?php echo $row['tiket_dewasa']?>" placeholder="Jumlah Tiket Dewasa" id="jum_dewasa-modal">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Anak</label>
                                    <input type="text" class="form-control" name="tiket_anak" value="<?php echo $row['tiket_anak']?>" placeholder="Jumlah Tiket Anak" id="jum_anak-modal">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Biaya Pertiket</label>
                                    <input type="text" class="per_tiket form-control" name="per_tiket" value="<?php echo $row['per_tiket']?>" placeholder="Total" id="per_tiket-modal">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">DP</label>
                                    <input type="text" class="dp form-control" name="dp" value="<?php echo $row['dp']?>" placeholder="dp" id="dp-modal">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Total Biaya</label>
                                    <input type="text" class="total form-control" name="total" value="<?php echo $row['total']?>" placeholder="Total" id="total-modal" readonly="readonly">
                              </div>
                              <div class="form-group col-md-6">
                                    <label for="birthday">Sisa</label>
                                    <input type="text" class="sisa form-control" name="sisa" value="<?php echo $row['sisa']?>" placeholder="Sisa" id="sisa-modal" readonly="readonly">
                              </div>
                          </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <input type="submit" class="btn btn-primary" name="update" value="Submit" >
                </div>
        </form>
      </div>

    </div>
  </div>
</div>
<?php endforeach; ?>
<?}?>

<?php if(!empty($this->session->flashdata('last_added'))){ ?>
<?php $no=0; foreach($this->session->flashdata('last_added') as $row): $no++; ?>
<div class="modal fade" id="modal_hapus<?php echo $row['id_tiket'];?>" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url(). 'tiket/Tiket/delete_tiket'; ?>">
            <input type="hidden" class="form-control" name="id" value="<?php echo $row['id_tiket'];?>"  />

                <div class="modal-body">
                    <p>Anda yakin mau menghapus <b><?php echo $row['nama'];?></b></p>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-danger">Hapus</button>
                </div>
            </form>
            </div>
            </div>
        </div>
<?php endforeach; ?>
<?}?>


<? $footer;
   $script;
?>
<script type="text/javascript">

$( '.per_tiket' ).mask('0.000.000.000', {reverse: true});

$( '.dp' ).mask('0.000.000.000', {reverse: true});

$( '.sisa' ).mask('0.000.000.000', {reverse: true});


  $(document).ready(function(){
  $('.hitung').keyup(function(){
      var anak = $("#jum_anak").val()
      var dewasa = $("#jum_dewasa").val()
      var pertiket = $("#per_tiket").val()
			var dp = $("#dp").val()
      var x =  pertiket.replace(/\./g,'');
			var y =  dp.replace(/\./g,'');
      
      if(anak == 1 || dewasa == 1){
        var total = (anak * x) + (dewasa * x);
      }else if(anak == 0){
        var total = x * dewasa;
      }else if(dewasa == 0){
        var total = x * anak;
      }else if(dewasa == 0 && anak == 0){
        var total = x;
      }else{
        var total = x * dewasa * anak;
      }
      var sisa = total - y;
      $('#total').val(total); 
      $('#sisa').val(sisa); 
    });
  })

  $(document).ready(function(){
  $('.hitung-modal').keyup(function(){
      var anak = $("#jum_anak-modal").val()
      var dewasa = $("#jum_dewasa-modal").val()
      var pertiket = $("#per_tiket-modal").val()
			var dp = $("#dp-modal").val()
      var x =  pertiket.replace(/\./g,'');
			var y =  dp.replace(/\./g,'');
      if(anak == 1 || dewasa == 1){
        var total = (anak * x) + (dewasa * x);
      }else if(anak == 0){
        var total = x * dewasa;
      }else if(dewasa == 0){
        var total = x * anak;
      }else if(dewasa == 0 && anak == 0){
        var total = x;
      }else{
        var total = x * dewasa * anak;
      }
			var sisa = total - y;
      $('#total-modal').val(total); 
      $('#sisa-modal').val(sisa); 
    });
  })


</script>